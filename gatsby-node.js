 const _ = require('lodash')
 const path = require('path')
 const { createFilePath } = require('gatsby-source-filesystem')

exports.createPages = ({ actions, graphql }) => {
  const { createPage, createRedirect } = actions

  createRedirect({
    fromPath: `/`,
    isPermanent: true,
    redirectInBrowser: true,
    toPath: `/be-nl/`,
  })
  return graphql(`{
    allWordpressPage {
      edges {
        node {
          id
          wpml_current_locale
          slug
          path
          wpml_translations {
            locale
          }
          template
        }
      }
    }
  }
  `
  )    
    .then(result => {
      if (result.errors) {
        result.errors.forEach(e => console.error(e.toString()))
        return Promise.reject(result.errors)
      }
    
     
      

      // In production builds, filter for only published pages.
  
    const allPages = result.data.allWordpressPage.edges
    const pages = allPages
    // Iterate over the array of pages
 
            _.each(pages, ({node: page}) => {
              const available_langs = []
              const pagetemp = page.template.split(".")[0] !== "" ? page.template.split(".")[0] : "page"
              const pathprefix = page.wpml_current_locale.toLowerCase().replace('_','-')
              if(page.wpml_translations.length !== 0){
                page.wpml_translations.forEach(el=>{
                  available_langs.push(el.locale)
                })
              }
            
            // Create the Gatsby page for this WordPress page
            if(pagetemp !== '404'){
            createPage({
                path: pagetemp !== `homepage` 
                ?                 
                `${pathprefix}/${pagetemp === 'email' 
                        ? 
                        `contact/${page.slug}` 
                        : 
                        page.slug}/` 
                :
                `${pathprefix}/`,
                component: path.resolve(`./src/templates/${pagetemp}.js`),
                context: {
                id: page.id,
                lang: page.wpml_current_locale,
                available_langs,
                linkpath: pagetemp === 'email' ? `contact/${page.slug}` : page.slug,
                pagetemp
                },
            })}
            })
    })
    .then(()=>{
      return graphql(`
      {
        allWordpressPost {
          edges {
            node {
              id
              wpml_current_locale
              slug
              path
              wpml_translations {
                locale
              }
            }
          }
        }
      }`)
    })
    .then(result=>{
      if (result.errors) {
        result.errors.forEach(e => console.error(e.toString()))
        return Promise.reject(result.errors)
      }
      const postTemplate = path.resolve(`./src/templates/post.js`)
      const allPosts = result.data.allWordpressPost.edges     
      const posts = allPosts
      
      _.each(posts, ({node: post}) => {
        const available_langs = []
        const pathprefix = post.wpml_current_locale.toLowerCase().replace('_','-')
        if(post.wpml_translations.length !== 0){
          post.wpml_translations.forEach(el=>{
            available_langs.push(el.locale)
          })
        }
        // Create the Gatsby page for this WordPress posts
        createPage({
            path: `/${pathprefix}/blog/${post.slug}/`,
            component: postTemplate,
            context: {
            id: post.id,
            lang: post.wpml_current_locale,
            available_langs,
            linkpath: `/blog/${post.slug}/`,
            },
        })
        })
    })
    .then(()=>{
      return graphql(`
      {
        allWordpressWpServices{
           edges {
            node {
              id
              wpml_current_locale
              acf{
                parent_services
              }              
              slug
              path
              wpml_translations {
                locale
              }
            }
          }
        }
      }`)
    })
    .then(result=>{
      if (result.errors) {
        result.errors.forEach(e => console.error(e.toString()))
        return Promise.reject(result.errors)
      }
      const serviceTemplate = path.resolve(`./src/templates/service.js`)
      const allServices = result.data.allWordpressWpServices.edges     
      const services = allServices
      
      _.each(services, ({node: service}) => {
        const available_langs = []
        const pathprefix = service.wpml_current_locale.toLowerCase().replace('_','-')
        if(service.wpml_translations.length !== 0){
          service.wpml_translations.forEach(el=>{
            available_langs.push(el.locale)
          })
        }
        // Create the Gatsby page for this WordPress posts
        createPage({
            path: !service.acf.parent_services || service.acf.parent_services === "none" ? `/${pathprefix}/${service.slug}/` : `/${pathprefix}/${service.acf.parent_services}/${service.slug}/`,
            component: serviceTemplate,
            context: {
            id: service.id,
            lang: service.wpml_current_locale,
            available_langs,
            linkpath: !service.acf.parent_services || service.acf.parent_services === "none" ? `/${service.slug}/` : `/${service.acf.parent_services}/${service.slug}/`,
            },
        })
        })
    })
    .then(()=>{
      return graphql(`{
        allWordpressWpCompanyprofile {
          edges {
            node {
              id
              wpml_current_locale
              slug
              path
              wpml_translations {
                locale
              }
            }
          }
        }
      }
      `)
    })
    .then(result =>{
      if (result.errors) {
        result.errors.forEach(e => console.error(e.toString()))
        return Promise.reject(result.errors)
      }
      const companyProfileTemplate = path.resolve(`./src/templates/company-profile.js`)
      const allCompanyProfiles = result.data.allWordpressWpCompanyprofile.edges     
      const profiles = allCompanyProfiles

      _.each(profiles, ({node: profile}) => {
        const available_langs = []
        const pathprefix = profile.wpml_current_locale.toLowerCase().replace('_','-')
        if(profile.wpml_translations.length !== 0){
          profile.wpml_translations.forEach(el=>{
            available_langs.push(el.locale)
          })
        }
        // Create the Gatsby page for this WordPress posts
        createPage({
            path: `/${pathprefix}/leveranciers/${profile.slug}/`,
            component: companyProfileTemplate,
            context: {
            id: profile.id,
            lang: profile.wpml_current_locale,
            available_langs,
            linkpath: `/leveranciers/${profile.slug}/`,
            },
        })
        })

    })
   
}

