import React, { useState,useEffect } from 'react'
import Layout from '../components/Layout'
import icon from '../components/assets/images/404.svg'
import RelatedService from '../components/service/relatedservice'
import PhoneBlock from '../components/home/phoneblock';
import logo from '../components/assets/images/icons/logo-color.svg'
import image from '../components/assets/images/homepage-phone-demo.png'
import { Helmet } from 'react-helmet';
import {Link} from 'gatsby'
const NotFoundPage = (props) => {
  const data = props.data && props.data.edges[1] && props.data.edges[1].node
  const [searchvalue, setSearchvalue] = useState('')
	const [searchfdata, setSearchfdata] = useState([])
	const handleChange =(e)=>{
		setSearchvalue(e.target.value)
		const dt = props.sdata.edges.filter(tts => tts.node.title.toLowerCase().includes(e.target.value.toLowerCase()) && (tts.node.acf.disable_in_search === null || tts.node.acf.disable_in_search.length ===0 ))
		setSearchfdata([...dt])
	}
	useEffect(()=>{


	},[])
  return(
    <Layout pagecontext={{lang:`be_NL`,available_langs:["be_NL","nl_NL"]}} searchbar={true}>
       <Helmet>
          <meta name="robots" content="noindex" />
          <meta name="googlebot" content="noindex" />
        </Helmet>
      <div className="page-four">

          <div className="container">

              <div className="row">
                  <div className="col-md-12">

                    <div className="thank-you-heading">
                        <h2 className="font-weight-600">{data && data.acf.heading}</h2>
                        <img src={icon}/>
                    </div>




                    <div className="phone-demo">
                    <div className="row justify-content-center mt-3 mb-5">
					
                      <div className="col-md-5">
                        <div className="home-mobile-content-area">
                          
                          <h2 className="font-weight-700 mb-4">Ofertes vergelijken</h2>
                          <h6 className="mb-4 font-weight-400">Zoek naar een dienstverlener of een vakman &
vergelijk hier gratis ofertes.</h6>
                          <div className="search-area">
                            <form className="my-2 my-lg-0">
                              <div className="search_icon">
                                <input
                                 className="form-control py-2 border-right-0 border" 
                                 type="search" 
                                 value={searchvalue} 
                                 autocomplete="off"
					                        onChange={handleChange}
                                 id="search" 
                                 placeholder="Zoeken.."/>
                                 {searchvalue !=='' && <ul className="search-list">
                            {searchfdata.length !==0 ? searchfdata.map((el,i)=>{
                              return <li id={el.node.id} key={i}><Link to={`/${el.node.wpml_current_locale.replace('_','-').toLowerCase()}/${el.node.slug}`}>{el.node.title}</Link></li>
                            }):<li>Niets gevonden</li>}
                            
                          </ul>}
                                <span className="search-icon-area">
                                  <i className="fa fa-search"></i>
                                </span>
                              </div>
                            </form>
                          </div>
                          <img src={logo} alt="logo" className="mb-4" width="100"/>
                        </div>
                      </div>

                      <div className="col-md-4">
                        <div className="mobile-img-home d-md-block d-none">
                          <img src={image} alt="homepage-phone-demo" className="img-fluid"/>
                        </div>
                      </div>
                    </div>

                    </div>



                    <RelatedService data={data} sdata={props.sdata}/>

                  </div>
              </div>

          </div>


       
      </div>
    </Layout>
    )
}
export default NotFoundPage

