import React, {useState, useEffect} from 'react'
import TemplateWrapper from '../components/Layout'
import LandingPage from '../components/landingpage/landing'
import {graphql} from 'gatsby';
import YoastData from '../components/yoast-data';
import Helmet from 'react-helmet'


export default (props)=>{

    const pagedata = props.data.wordpressPage
    useEffect(()=>{

    },[])
    return(
        <TemplateWrapper pagecontext={props.pageContext} searchbar = {true}>
          <Helmet>
            <body className="landing-page"/>
          </Helmet>  
        <YoastData data={pagedata ? pagedata.yoast : null}/> 
			<LandingPage 
      aff={pagedata.acf.banner_image !== null ? pagedata.acf.banner_image : null}
      phone={pagedata.acf.banner_app_image !== null ? pagedata.acf.banner_app_image : null}
			heading={pagedata.acf.banner_heading}
			subheading={pagedata.acf.banner_subheading}
			bannerbuttontext={pagedata.acf.banner_button_text}

			servicetitle={pagedata.acf.second_block_heading}
			servicesubtitle={pagedata.acf.second_block_subheading}

			servicebox1_icon={pagedata.acf.box_1_icon !== null ? pagedata.acf.box_1_icon:null}
			servicebox1_title={pagedata.acf.box_1_title}
			servicebox1_description={pagedata.acf.box_1_decription}

			servicebox2_icon={pagedata.acf.box_2_icon !== null ? pagedata.acf.box_2_icon:null}
            servicebox2_title={pagedata.acf.box_2_title}
			servicebox2_description={pagedata.acf.box_2_description}

			servicebox3_icon={pagedata.acf.box_3_icon !== null ? pagedata.acf.box_3_icon:null}
            servicebox3_title={pagedata.acf.box_3_title}
			servicebox3_description={pagedata.acf.box_3_description}

			callactiontitle={pagedata.acf.third_block_heading}
			callactionsubtitle={pagedata.acf.third_block_subheading}

			infotitle={pagedata.acf.fourth_block_heading}
			infodescription={pagedata.acf.fourth_block_subheading}
			infobuttontext={pagedata.acf.fourth_block__button_text}
			infobuttonaftertext={pagedata.acf.fourth_block_button_after_text}
			
			/>

        </TemplateWrapper>
    )
}



export const pageData = graphql`
query PageAfflandingpage($id: String!){
    wordpressPage(id: {eq: $id}) {
      title
      type
      status
      yoast {
        focuskw
        title
        metadesc
        linkdex
        metakeywords
        meta_robots_noindex
        meta_robots_nofollow
        meta_robots_adv
        canonical
        redirect
        opengraph_title
        opengraph_description
        opengraph_image
        twitter_title
        twitter_description
        twitter_image
      }
      wpml_current_locale
      acf {
        banner_heading
        banner_button_text
        banner_after_button_text
        banner_subheading
        banner_contact_text
        banner_timing_text
        second_block_heading
        second_block_subheading
        box_1_title
        box_1_decription
        box_2_title
        box_2_description
        box_3_title
        box_3_description
        third_block_heading
        third_block_subheading
        fourth_block_heading
        fourth_block_subheading
        fourth_block__button_text
        fourth_block_button_after_text
        search_placeholder_text
        search_button_text
        popular_text
        app_heading
        app_subheading
        app_button_text
        app_button_link
        our_client_heading
        call_to_action_text
        call_to_action_button_text
        call_to_action_button_link
        banner_background_image {
          source_url
          title
        }
        banner_image {
          title
          source_url
        }
        box_1_icon {
          title
          source_url
        }
        box_2_icon {
          source_url
          title
        }
        box_3_icon {
          title
          source_url
        }
        fourth_block_image {
          source_url
          title
        }
        banner_app_image {
          source_url
          title
        }
      }
    }
   
  }`