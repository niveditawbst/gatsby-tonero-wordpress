import React, {useState, useEffect } from 'react'
import TemplateWrapper from '../components/Layout'
import ServiceSearch from '../components/home/servicesearch'
import PhoneBlock from '../components/home/phoneblock';
import CallToAction from '../components/home/calltoaction';
import OurClient from '../components/home/ourclient';
import {graphql} from 'gatsby';
import { ServiceCategoryList } from '../components/home/servicecategorylist';
import YoastData from '../components/yoast-data';
import Helmet from 'react-helmet'
import HomeIcons from '../components/home/homeicons';
import HowItWorks from '../components/home/howitworks';
import LatestBlog from '../components/home/latestblog';
import { SkeletonTheme } from 'react-loading-skeleton';


export default (props)=>{
  const [allcat, setAllcat] = useState(null)
  const [servicedata, setServicesdata] = useState([])
	const pagedata = props.data.wordpressPage
  const clientdata = props.data.allWordpressWpClient
  const servicesdata = props.data.allWordpressWpServices
  const blogs = props.data.allWordpressPost
  const langurl = props && props.pageContext ? props.pageContext.lang.toLowerCase().replace('_','-') : null
	useEffect(()=>{	   
		if(allcat === null && pagedata !== null){	
		let uniqcat = []
		const catdata = []
	  const categories = []
		servicesdata.edges.forEach(el => {
			el.node.acf.duplicate_categories.forEach(sr=>{
				categories.push(sr)
			})
		});
		uniqcat = categories.filter(function(item, pos, self) {
			return self.indexOf(item) == pos;
		})
		
		uniqcat.forEach(elet=>{		
			let flser = servicesdata.edges.filter(tl=>tl.node.acf.duplicate_categories.includes(elet))
			let obj = {
				catname:elet,
				catservice:flser
			}
			 catdata.push(obj)

		})
    setAllcat([...catdata])
  
	}

	},[])
    return(
    <TemplateWrapper pagecontext={props.pageContext} setServicesdata={setServicesdata}>	
    <SkeletonTheme color="#c5c5c5" highlightColor="#9d9d9d">
        <Helmet>
          <body className="home-page"/>
        </Helmet>

       <YoastData data={pagedata ? pagedata.yoast : null}/> 
		<ServiceSearch 
		title={pagedata !==null && pagedata.acf.banner_heading}
		subtitle={pagedata !==null && pagedata.acf.banner_subheading}
		background={pagedata !==null && pagedata.acf.banner_background_image}
		search_placeholder={pagedata !==null && pagedata.acf.search_placeholder_text}
		search_btn_text={pagedata !==null && pagedata.acf.search_button_text}
    after_searchfield_text={pagedata !==null && pagedata.acf.popular_text}
    servicedata={servicedata}
		/>

    <HomeIcons data={pagedata !==null && pagedata.acf}/>
        <main className="main-content bg-grey">
		<div className="container">
			<div className="row pt-5">
				<div className="col-lg-12">

          <div className="home-service-title">

            <h2 className="font-weight-600">{pagedata.acf.service_title}</h2>
            <p className="font-weight-400">{pagedata.acf.service_subtitle}</p>

          </div>


						<div className="home-images-area voor-area mb-5">
							{allcat !== null 
							&&
							allcat.map(el=>{
								return <ServiceCategoryList langurl={langurl} catname={el.catname} catservice={el.catservice}/>
							})}
						</div>	

					</div>
				</div>


        <HowItWorks data={pagedata !==null && pagedata.acf}/>

				<div className="phone-demo">
					<PhoneBlock 
					title={pagedata !==null && pagedata.acf.app_heading}
					image={pagedata !==null && pagedata.acf.app_image.source_url}
					logo={pagedata !==null && pagedata.acf.app_logo.source_url}
					description={pagedata !==null && pagedata.acf.app_subheading}
					btn_text={pagedata !==null && pagedata.acf.app_button_text}
          btn_link={pagedata !==null && pagedata.acf.app_button_link}
          servicedata={servicedata}
					/>
				</div>



				<div className="companies-logo-area mt-3 text-center mb-5">
					<h3 className="font-weight-700 gray-text mb-5 text-hover">
						{pagedata !==null && pagedata.acf.our_client_heading}
						</h3>
					<OurClient clients={clientdata ? clientdata.edges : null}/>
				</div>
			</div>


      <LatestBlog data={blogs} langurl={langurl}/>



			<div className="registreren-box home-registeration-box py-4 px-4">
				<div className="container">
					<CallToAction 
					title={pagedata !==null && pagedata.acf.call_to_action_text}
					btn_text={pagedata !==null && pagedata.acf.call_to_action_button_text}
					btn_link={pagedata !==null && pagedata.acf.call_to_action_button_link}
					/>
				</div>
			</div>
	</main>

  </SkeletonTheme>
        </TemplateWrapper>
    )
}



export const pageQuery = graphql`
query PageByID($id: String!, $lang: String!){
  wordpressPage(id: {eq: $id}) {
    title
    type
    status
    wpml_current_locale
    yoast {
      focuskw
      title
      metadesc
      linkdex
      metakeywords
      meta_robots_noindex
      meta_robots_nofollow
      meta_robots_adv
      canonical
      redirect
      opengraph_title
      opengraph_description
      opengraph_image
      twitter_title
      twitter_description
      twitter_image
    }
    acf {
      banner_heading
      banner_button_text
      banner_after_button_text
      banner_subheading
      banner_contact_text
      banner_timing_text
      second_block_heading
      second_block_subheading
      box_1_title
      box_1_decription
      box_2_title
      box_2_description
      box_3_title
      box_3_description
      third_block_heading
      third_block_subheading
      fourth_block_heading
      fourth_block_subheading
      fourth_block__button_text
      fourth_block_button_after_text
      search_placeholder_text
      search_button_text
      popular_text
      app_heading
      app_subheading
      app_button_text
      app_button_link
      our_client_heading
      call_to_action_text
      call_to_action_button_text
      call_to_action_button_link
      app_image {
        source_url
      }
      app_logo {
        source_url
      }
      banner_app_image {
        source_url
      }
      banner_background_image {
        source_url
      }
      banner_image {
        source_url
      }
      box_1_icon {
        source_url
      }
      box_2_icon {
        source_url
      }
      box_3_icon {
        source_url
      }
      fourth_block_image {
        source_url
      }
      featured_icon_one{
        source_url
      }
      featured_heading_one
      featured_subheading_one
      featured_icon_two{
        source_url
      }
      featured_heading_two
      featured_subheading_two
      featured_icon_three{
        source_url
      }
      featured_heading_three
      featured_subheading_three
      featured_icon_four{
        source_url
      }
      featured_heading_four
      featured_subheading_four
      service_title
      service_subtitle
      how_it_work_heading
      how_it_work_image_one{
        source_url
      }
      how_it_work_title_one
      how_it_work_subtitle_one
      how_it_work_image_two{
        source_url
      }
      how_it_work_title_two
      how_it_work_subtitle_two
      how_it_work_image_three{
        source_url
      }
      how_it_work_title_three
      how_it_work_subtitle_three
    }
  }
  allWordpressWpClient(filter: {wpml_current_locale: {eq: $lang}}) {
    edges {
      node {
        id
        acf {
          client_logo_image {
			source_url
			title
          }
        }
      }
    }
  }
  allWordpressWpServices(filter: {wpml_current_locale: {eq: $lang}}) {
    edges {
      node {
        id
        title
        acf {
          disable_in_search
          parent_services
          services_image {
            title
            source_url
          }
          duplicate_categories
        }
        slug
      }
    }
  }
  allWordpressPost(filter: {wpml_current_locale: {eq: $lang}}) {
    edges {
      node {
        id
        title
        featured_media {
          source_url
          title
        }
        excerpt
        slug
        categories {
          name
        }
        date(fromNow: true)
      }
    }
  }
}
`