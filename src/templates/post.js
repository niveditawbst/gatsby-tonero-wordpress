import React,{useState,useEffect} from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import { graphql, Link } from 'gatsby'
import Layout from '../components/Layout'
import BlogSidebar from '../components/blog/sidebar'
import BlogForm from '../components/blog/form'
import Content,{HTMLContent} from '../components/Content'
import { ServiceStepPopup } from '../components/servicesteppopup'
import YoastData from '../components/yoast-data'
import LatestBlog from '../components/home/latestblog'
import ReleventArticle from '../components/blog/releventarticle'
import RelatedService from '../components/service/relatedservice'
export const BlogPostTemplate = ({
  content,
  category,
  categories,
  featuredimage, 
  title, 
  date, 
  fimage,
  pageContext,
  serviceFormTitle,
  serviceFor}) =>{

    const [activepopup, setActivepopup] = useState(0)
    const [stepformdata, setStepformdata] = useState(null)
    const [serviceformdata,setServiceformdata] = useState([])

  const createjson = (data) => {
    return JSON.stringify(data)
  }
  const handleServicePopup = (e) =>{
    if(e === `next`){
      setActivepopup(activepopup + 1)
    }else if(e === `prev`){
      setActivepopup(activepopup - 1)
    }else if(e === `close`){
      setActivepopup(0)
    }else{
      setActivepopup(e)
    } 
  }
  const handleServiceFormSubmittedData = async(e)=>{
   if(!e.submit){
    setServiceformdata({...serviceformdata, [e.form]:e.data})  
   }else{
    // setServiceformdata({...serviceformdata, [e.form]:e.data}) 
    const ltr = Object.values({...serviceformdata, [e.form]:e.data})   

    let bodydata = createjson({"gatsbyinput":{
      "main_title" : "Blog service form data",
      "main_data" : ltr
    }})
     await fetch("https://backend.production.tonero.eu/wp-json/form/v1/submit", {
      method: "post",
      headers: {"Content-Type": "application/json; charset=UTF-8" },
      body: bodydata
    })
      .then((res) => {    
        console.log(res,'response')       
      })
      .catch(error => {
        console.log(error,'responsee')       
      }); 
   }
  }
  const handleServicesStep = (e) =>{
   if(e !== ""){
    setActivepopup(1)
    setStepformdata(e)
   }
  }
useEffect(()=>{
  
},[activepopup,serviceformdata])
  // const [category, setCategory] = useState(null)
  // useEffect(()=>{
  //   if(category === null && categories){
  //     let ft = []
  //     categories.forEach(el => {
  //       ft.push(el.node.name)
  //     });
  //     setCategory(ft.join(', '))
  //   }
  // },[category])
  return(    
    <React.Fragment>
      <Helmet>
          <body className="blog-detail-single"/>
      </Helmet>
    <main className="main-content blog-single-content-page">
    {/* <div className="container">
    
      <div className="header-bg blog-page-bg" style={{backgroundImage:`url(${fimage})`}}>
        <div className="header-content">

        </div>
      </div>
    </div> */}
    <div className="container mbl-remove-pd">
    {/* <div className="border-bottom mt-5 mb-5"></div> */}
    <div className="blog-single-area mt-5 mb-5">
    <div className="row">
      <div className="col-lg-8 mbl-remove-pd">
        <div className="blog-single-area blog-bg-white">

        <div className="post-img mob-show">
            {featuredimage !== null 
            && 
            <img src={featuredimage} alt="blog-img" className="img-fluid mb-4"/>}
          </div>


          <div className="m-mbl-show">
          <p className="highlight-text mb-0 font-weight-600">
            <HTMLContent content={category}/>
            </p>
          <div className="d-sm-flex justify-content-between">
            <p className="date font-weight-600 mb-1">
              {date}
            </p>
            <div className="blog-single-social">
              <ul className="social-icons mb-1">
                <li>
                  <a href="mailto:info@tonero.com" className="email">
                  <i className="fas fa-envelope"></i>
                  </a>
                </li>
                <li><a href="#" className="pinterest"><i className="fab fa-pinterest-square"></i></a></li>
                <li><a href="#" className="facebook"><i className="fab fa-facebook-square"></i></a></li>
                <li><a href="#" className="twiiter"><i className="fab fa-twitter-square"></i></a></li>
                <li><a href="#" className="linkedin"><i className="fab fa-linkedin"></i></a></li>
              </ul>
            </div>
          </div>

          </div>



      <h2 className="font-weight-800 mb-2 post-title"><HTMLContent content={title}/>  </h2>


      <div className="m-mbl-hide">
          <p className="highlight-text mb-0 font-weight-600">
            <HTMLContent content={category}/>
            </p>
          <div className="d-sm-flex justify-content-between">
            <p className="date font-weight-600 mb-1">
              {date}
            </p>
            <div className="blog-single-social">
              <ul className="social-icons mb-1">
                <li>
                  <a href="mailto:info@tonero.com" className="email">
                  <i className="fas fa-envelope"></i>
                  </a>
                </li>
                <li><a href="#" className="pinterest"><i className="fab fa-pinterest-square"></i></a></li>
                <li><a href="#" className="facebook"><i className="fab fa-facebook-square"></i></a></li>
                <li><a href="#" className="twiiter"><i className="fab fa-twitter-square"></i></a></li>
                <li><a href="#" className="linkedin"><i className="fab fa-linkedin"></i></a></li>
              </ul>
            </div>
          </div>

          </div>



          <div className="post-img mob-hide">
            {featuredimage !== null 
            && 
            <img src={featuredimage} alt="blog-img" className="img-fluid mb-4"/>}
          </div>



          <div className="post-content mt-2">
            <HTMLContent content={content}/>       
          </div>




            <div className="mbl-social">
          <div className="blog-single-social">
              <ul className="social-icons mb-1">
                <li>
                  <a href="mailto:info@tonero.com" className="email">
                  <i className="fas fa-envelope"></i>
                  </a>
                </li>
                <li><a href="#" className="pinterest"><i className="fab fa-pinterest-square"></i></a></li>
                <li><a href="#" className="facebook"><i className="fab fa-facebook-square"></i></a></li>
                <li><a href="#" className="twiiter"><i className="fab fa-twitter-square"></i></a></li>
                <li><a href="#" className="linkedin"><i className="fab fa-linkedin"></i></a></li>
              </ul>
            </div>

            </div>



        </div>


      <div className="mbl-form">
      <div className="widget select-filter-area mb-4 search-filter-posts-widget">
      <span className="gr-button">VERGELIJK OFFERTES</span>
          {serviceFormTitle !== null 
          && 
          
          <div className="select-box p-4">
                <h5 className="text-white font-weight-600 mb-4">
                  {serviceFormTitle}
                </h5>
                {serviceFor !== null 
                && 
                <div className="filter-btns">
                  {serviceFor.map((el, i)=>{
                    return (<button key = {`df${i+1}`} 
                            className="btn btn-install" 
                            onClick={()=>handleServicesStep(el.step_forms)} >
                            <p>{el.title}</p>
                            <span><i className="fas fa-angle-right"></i></span>
                            </button>)
                  })}
                    
                    
                </div>}
            </div>}                     
        </div>
      </div>


      <div className="single-latest-blog">
          <LatestBlog />
      </div>



    <div className="single-related-blog">
    <RelatedService/>
    </div>
      </div>











      <div className="col-lg-4">    
      <BlogSidebar categories={categories}/>
      <ReleventArticle />



      <div className="widget select-filter-area d-lg-block d-none mb-4 search-filter-posts-widget">
      <span className="gr-button">VERGELIJK OFFERTES</span>
          {serviceFormTitle !== null 
          && 
         
          <div className="select-box p-4">
                <h5 className="text-white font-weight-600 mb-4">
                  {serviceFormTitle}
                </h5>
                {serviceFor !== null 
                && 
                <div className="filter-btns">
                  {serviceFor.map((el, i)=>{
                    return (<button key = {`df${i+1}`} 
                            className="btn btn-install" 
                            onClick={()=>handleServicesStep(el.step_forms)} >
                            <p>{el.title}</p>
                            <span><i className="fas fa-angle-right"></i></span>
                            </button>)
                  })}
                    
                    
                </div>}
            </div>}                     
        </div>




      </div>
    </div>
    </div>
    </div>
    </main>
	{activepopup > 0 
    && 
    <ServiceStepPopup 
    pageContext={pageContext}
    activepopup={activepopup} 
    handleServicePopup={handleServicePopup} 
    stepformdata={stepformdata} 
    handleServiceFormSubmittedData={handleServiceFormSubmittedData}/>}
    </React.Fragment>

  )
}

const BlogPost = (props) => {
   const post = props.data.wordpressPost
   const sdata = props.data.allWordpressWpSingleblog.edges[0].node.acf
  return (
    <Layout pagecontext={props.pageContext} searchbar = {true}>
      <YoastData data={post ? post.yoast : null}/> 

      <BlogPostTemplate
        content={post.content}
        category={props.data.wordpressCategory.name}
        categories={props.data.allWordpressCategory.edges}
        pageContext={props.pageContext} 
        serviceFormTitle={post.acf.service_form_title}
        serviceFor={post.acf.service_for}
        featuredimage={post.featured_media !== null ? post.featured_media.source_url : null}
        title={post.title}
        date={post.date}
        fimage={sdata.banner_image.source_url}
      />
    </Layout>
  )
}

export default BlogPost

export const pageQuery = graphql`
query BlogPostByID($id: String!, $lang:String!){
  wordpressPost(id: {eq: $id}) {
    id
    content
    excerpt
    title
    yoast {
      focuskw
      title
      metadesc
      linkdex
      metakeywords
      meta_robots_noindex
      meta_robots_nofollow
      meta_robots_adv
      canonical
      redirect
      opengraph_title
      opengraph_description
      opengraph_image
      twitter_title
      twitter_description
      twitter_image
    }
    acf {
      service_form_title
      service_for {
        step_forms
        title
      }
    }
    featured_media {
      source_url
      title
    }
    date(fromNow: true)
    comment_status
    type
  }
  wordpressCategory {
    name
    id
  }
  allWordpressCategory{
    edges {
      node {
        id
        name
      }
    }
  }
  allWordpressWpSingleblog(filter: {wpml_current_locale: {eq: $lang}}) {
    edges {
      node {
        id
        acf {
          banner_image {
            source_url
            title
          }
        }
      }
    }
  }
}`
