import React, {useState,useEffect} from 'react'
import TemplateWrapper from '../components/Layout'
import blog from '../components/assets/images/blog.jpg';
import blogbg from '../components/assets/images/blog-cover.jpg';
import SubscribeForm from '../components/blog/subscribeform';
import BlogSidebar from '../components/blog/sidebar';
import Content,{HTMLContent} from '../components/Content' 
import {graphql,Link} from 'gatsby';
import YoastData from '../components/yoast-data';
import Helmet from 'react-helmet'
export default (props)=>{
    const langurl = props && props.pageContext ? props.pageContext.lang.toLowerCase().replace('_','-') : null
    const pagedata = props && props.data ? props.data.wordpressPage : null
    const postdata = props && props.data ? props.data.allWordpressPost: null
    const categories = props && props.data ? props.data.allWordpressCategory.edges :null
    const [mostreadblog,setMostreadblog] = useState(null)
	// const bdata = null
        useEffect(()=>{
         if(!mostreadblog){
          const dtt = pagedata.acf.popular_posts.split(",")
          const readblogdata=[]
          dtt.forEach(el=>{
            const bdt = postdata.edges.filter(ell=>ell.node.wordpress_id.toString() === el.split("-")[0])
            let obj={
              id: el.split("-")[0],
              title:bdt[0].node.title,
              image:bdt[0].node.featured_media,
              slug:bdt[0].node.slug,
              count: el.split("-")[1]
            }
            readblogdata.push(obj)
           
          })
          setMostreadblog([...readblogdata])
         }
        },[])

    return(
        <TemplateWrapper pagecontext={props.pageContext} searchbar = {true}>  
         <Helmet>
          <body className="blog-list"/>
        </Helmet>
         <YoastData data={pagedata ? pagedata.yoast : null}/> 
        	<main className="main-content blog-content-page">
          <div className="container mbl-remove-pd">
                <div 
                className="header-bg blog-page-bg" 
                style={{backgroundImage:`url(${pagedata !==null && pagedata.acf.banner_image !== null ? pagedata.acf.banner_image.source_url : null})`}}>
	            	<div className="header-content">
               
	            		<div className="row">
	            			<SubscribeForm 
							heading={pagedata !==null ?  pagedata.acf.banner_heading : null}
							buttontext={pagedata !==null ?  pagedata.acf.banner_button_text : null}
							buttonaftertext={pagedata !==null ?  pagedata.acf.banner_after_button_text : null}
							/>
			            </div>
	            	</div>
	            </div>
            </div>




		<div className="container">
			

			<div className="blog-categories mb-4">
      <h5 className="font-weight-600 mb-4 widget-title mbl-show">CATEGORIEËN</h5>
	                  <div className="cat-list">
                    
	                    <ul className="navbar-nav px-lg-0 px-4">
	                      <li className="nav-item active">
	                        <a className="nav-link pl-0" href="#">Laatste berichten </a>
	                      </li>
	                      <li className="nav-item">
	                        <a className="nav-link" href="#">Woongids</a>
	                      </li>
	                      <li className="nav-item">
	                        <a className="nav-link" href="#">Bereken zonnepanelen rendement</a>
	                      </li>
	                      <li className="nav-item">
	                        <a className="nav-link" href="#">Muren schilderen: kleuren kiezen (tips)</a>
	                      </li>
	                      <li className="nav-item">
	                        <a className="nav-link" href="#">Vakmannen vinden</a>
	                      </li>
	                      <li className="nav-item">
	                        <a className="nav-link" href="#">Energiezuinigen</a>
	                      </li>
	                    </ul>
	                  </div>

			</div>
			<div className="blog-grid-area">
				<div className="row">
					<div className="col-md-8">
						<h4 className="font-weight-700 mb-4 messages-title">Laatste berichten</h4>
						<div className="row">


                        {postdata !== null 
                        &&
                        postdata.edges.map((el, i) => {
                        return(
                            <div className="col-md-6">
                                <div className="blog-items mb-5">
                                    <div className="item">
                                        <Link to={`/${langurl}/blog/${el.node.slug}`}>
                                            <img 
                                            src={el.node.featured_media !== null 
                                                &&
                                                 el.node.featured_media.source_url} 
                                            alt={el.node.featured_media !== null 
                                                &&
                                                 el.node.featured_media.title} 
                                            className="img-fluid"/>
                                        </Link>
                                        <div className="mbl-design">
                                        <h5 className="font-weight-500 post-title mt-4">
                                            <Link to={`/${langurl}/blog/${el.node.slug}`}>
                                            <HTMLContent content={el.node.excerpt}/>
                                                
                                            </Link> </h5>										
                                        <h5 className="font-weight-600 post-des mb-1">
                                            <Link to={`/${langurl}/blog/${el.node.slug}`}>
                                            <HTMLContent content={el.node.title}/>
                                            </Link>
                                        </h5>
                                        <p className="date font-weight-600 small-font">
                                            {el.node.date}
                                        </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            )
							})}

							





						
						</div>
					</div>
					<div className="col-md-4">
						<BlogSidebar mostreadblog={mostreadblog} langurl={langurl}/>
					</div>
				</div>
				{/* <div className="row justify-content-center mb-5 mt-4 pb-3">
					<div className="col-12 text-center">
						<div className="pagination-area">
							<ul className="paginations">
								<li className=""><a href="#">1</a></li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#">4</a></li>
								<li><a href="#">5</a></li>
							</ul>
						</div>
					</div>
				</div> */}
			</div>
		</div>
	</main>

        </TemplateWrapper>
    )
}







export const blogPageQuery = graphql`
query blogPageQuery($id:String!, $lang:String!){
    wordpressPage(id: {eq: $id}) {
        title
        wpml_current_locale
        yoast {
          focuskw
          title
          metadesc
          linkdex
          metakeywords
          meta_robots_noindex
          meta_robots_nofollow
          meta_robots_adv
          canonical
          redirect
          opengraph_title
          opengraph_description
          opengraph_image
          twitter_title
          twitter_description
          twitter_image
        }
        acf {
          banner_heading
          popular_posts
          banner_button_text
          banner_after_button_text
          banner_image {
            source_url
            title
          }
        }
      }
      allWordpressCategory{
        edges {
          node {
            id
            name
          }
        }
      }
      allWordpressPost(filter: {wpml_current_locale: {eq: $lang}}) {
        edges {
          node {
            id
            wordpress_id
            title
            featured_media {
              source_url
              title
            }
            excerpt
            slug
            categories {
              name
            }
            date(fromNow: true)
          }
        }
      }
}`


