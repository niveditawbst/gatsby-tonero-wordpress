import React,{useState,useEffect} from 'react'
import TemplateWrapper from '../components/Layout'
import { Contact, Supplier } from '../components/service/contact'
import { ServiceContactForm } from '../components/contact-forms/service-contact-form'
import YoastData from '../components/yoast-data'

export default (props)=>{
    const [status, setStatus] = useState(false)
    const data = props && props.data && props.data.wordpressPage ? props.data.wordpressPage : null
	useEffect(()=>{
		if(!status){
			setStatus(true)
        }
 
	},[status])
    return(
        <TemplateWrapper pagecontext={props.pageContext} searchbar = {true}>
              <YoastData data={data ? data.yoast : null}/> 
        <main className="main-content">

      <div className="email-container">

          <div className="email-header">
            <div className="text-center">
                <h3 className="font-weight-700">{data.acf.heading}</h3>
                <p className="font-weight-500">{data.acf.subheading}</p>
            </div>
          </div>

		<div className="container">
			<div className="row justify-content-center">
				<div className="col-xl-8 col-lg-10 col-12">
						<div className="register-page-area klantenservice-area">
                           <p className="mbl-s font-weight-500">{data.acf.subheading}</p>
							<div className="register-form">
								{status && <ServiceContactForm data = {data}/>}				
									{/* <div className="address-boxes">
										<div className="row justify-content-center">
											
                                            <Contact data = {data}/>

											<div className="col-md-1"></div>

                                            <Supplier data = {data}/>
											
										</div>
									</div> */}
									<div className="helo-text-register text-center mb-2">
										<p>
                      {/* <span className="bg-white mr-3 d-inline-block">
                                            <i className="far fa-question-circle"></i></span>  */}
                                           {data.acf.help_text}
                                            <a href="#"> here</a></p>
									</div>
                                    
							
							</div>
						</div>					
					</div>
				</div>
			</div>

            <div className="email-footer">

            </div>
            </div>
	</main>



        </TemplateWrapper>
    )
}



export const pageQuery = graphql`
query ContactForm($id:String!) {
    wordpressPage(id: {eq: $id}) {
        id
        title
        yoast {
            focuskw
            title
            metadesc
            linkdex
            metakeywords
            meta_robots_noindex
            meta_robots_nofollow
            meta_robots_adv
            canonical
            redirect
            opengraph_title
            opengraph_description
            opengraph_image
            twitter_title
            twitter_description
            twitter_image
          }
        acf {
          heading
          subheading
          first_name_label
          first_name_placeholder
          last_name_label
          last_name_placeholder
          email_label
          email_placeholder
          phone_label
          phone_placeholder
          message_label
          message_placeholder
          button_text
          contact_details_heading
          contact_details_info
          supplier_heading
          supplier_heading_info
          help_text
         
        } 
      }
}`  