import React, { useEffect } from 'react'
import TemplateWrapper from '../components/Layout'
import YoastData from '../components/yoast-data'
import icon from '../components/assets/images/thankyou.svg'
import Helmet from 'react-helmet'
import RelatedService from '../components/service/relatedservice'
import Faq from '../components/thankyou/faq'

const ThankYou =(props)=> {
    const data = props.data.wordpressPage
    const faqdata = props.data && props.data.allWordpressWpFaq ? props.data.allWordpressWpFaq.edges : null
    useEffect(()=>{

    },[props.data])
    return(
        <TemplateWrapper pagecontext={props.pageContext} searchbar={true}>
           <Helmet>
          <meta name="robots" content="noindex" />
          <meta name="googlebot" content="noindex" />
          <body className="thanku"/>
        </Helmet>
          <YoastData data={data ? data.yoast : null}/> 

            <div className="thank-you-page">

                <div className="container">

                    <div className="row">

                        <div className="col-md-12">

                            {/* <div className="thank-you-box shadow-sm" >
                                <div dangerouslySetInnerHTML={{ __html: data.content }}/>
                            </div> */}

                            <div className="thank-you-heading">
                              <h3 className="font-weight-400">{data.acf.heading} <span>#485759</span></h3>
                              <h2 className="font-weight-600">{data.acf.subheading}</h2>
                              <img src={data.acf.icon.source_url}/>
                            </div>


                            <Faq data={faqdata[0].node.acf}/> 


                        
                            <RelatedService data={data}/>



                        </div>

                    </div>

                </div>

            </div>

        </TemplateWrapper>
    )
}


export default ThankYou;

export const pageQuery = graphql`
  query thanksPageById($id: String!,$lang:String!) {
    wordpressPage(id: { eq: $id }) {
      title
      content
      acf{
        heading
        subheading
        icon{
          source_url
        }
        related_service_title
        footer_contact_text
      }
      yoast {
        focuskw
        title
        metadesc
        linkdex
        metakeywords
        meta_robots_noindex
        meta_robots_nofollow
        meta_robots_adv
        canonical
        redirect
        opengraph_title
        opengraph_description
        opengraph_image
        twitter_title
        twitter_description
        twitter_image
      }
    }
    allWordpressWpFaq(filter: {wpml_current_locale: {eq: $lang}}) {
      edges {
        node {
          id
          title
          acf {
            heading
            faq_list {
              answer
              question
            }
          }
          wpml_translations {
            locale
          }
        }
      }
    }
  }
`
