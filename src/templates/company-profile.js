import React from 'react'
import TemplateWrapper from '../components/Layout'
import CompanyProfile from '../components/company-profile'
import { graphql } from 'gatsby'
import YoastData from '../components/yoast-data'
import { Helmet } from 'react-helmet'

export default (props)=>{
    
    const data = props && props.data && props.data.wordpressWpCompanyprofile ? props.data.wordpressWpCompanyprofile : null
    return (
        <TemplateWrapper pagecontext={props.pageContext} searchbar={true}>
           <Helmet>
          <meta name="robots" content="noindex" />
          <meta name="googlebot" content="noindex" />
        </Helmet>
          <YoastData data={data ? data.yoast : null}/> 
            <CompanyProfile 
            data={data} 
            pageContext={props.pageContext} 
            serviceFormTitle={data.acf.service_form_title}
            serviceFor={data.acf.service_for}
            />
        </TemplateWrapper>
        )
}

export const pageQuery = graphql`
query CompanyProfilePage($id:String!) {
    wordpressWpCompanyprofile(id: {eq: $id}) {
      id
      title
      yoast {
        focuskw
        title
        metadesc
        linkdex
        metakeywords
        meta_robots_noindex
        meta_robots_nofollow
        meta_robots_adv
        canonical
        redirect
        opengraph_title
        opengraph_description
        opengraph_image
        twitter_title
        twitter_description
        twitter_image
      }
      acf {
        company_name
        address
        contact_button_text
        tab_one
        tab_two
        tab_three
        about_heading
        about_subheading
        about_description
        business_heading
        map
        service_form_title
          service_for {
            step_forms
            title
          }
        company_logo {
          source_url
          title
        }
      }
    }
  }
  `