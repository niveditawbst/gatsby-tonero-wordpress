import React, { useEffect } from 'react'
import TemplateWrapper from '../components/Layout'
import YoastData from '../components/yoast-data'



const Privacy =(props)=> {
    const data = props.data.wordpressPage
    return(
        <TemplateWrapper pagecontext={props.pageContext} searchbar={true}>
           <YoastData data={data ? data.yoast : null}/> 
            <div className="terms-page">

                <div className="container">

                    <div className="row">

                        <div className="col-md-12">

                            <div className="terms-box shadow-sm" dangerouslySetInnerHTML={{ __html: data.content }}>


                      
                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </TemplateWrapper>
    )
}


export default Privacy;


export const pageQuery = graphql`
  query termsPageById($id: String!) {
    wordpressPage(id: { eq: $id }) {
      title
      content
      yoast {
        focuskw
        title
        metadesc
        linkdex
        metakeywords
        meta_robots_noindex
        meta_robots_nofollow
        meta_robots_adv
        canonical
        redirect
        opengraph_title
        opengraph_description
        opengraph_image
        twitter_title
        twitter_description
        twitter_image
      }
    }
  }
`
