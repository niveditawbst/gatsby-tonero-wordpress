import React,{useState,useEffect, useRef} from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import { graphql, Link } from 'gatsby'
import Layout from '../components/Layout'
import BlogSidebar from '../components/blog/sidebar'
import BlogForm from '../components/blog/form'
import {HTMLContent} from '../components/Content'
import {ServiceStepPopup} from '../components/servicesteppopup'
import Skeleton,{SkeletonTheme} from 'react-loading-skeleton';
import UpsellServiceList from '../components/upsell-service'
import UpsellServiceListt from '../components/upsell-service/upsell-service-sub'
import YoastData from '../components/yoast-data'
import Faq from '../components/thankyou/faq'
export const SingleServiceTemplate = ({
    faqdata,
    pageContext,
    serviceFormTitle,
    serviceFor,
    suggestedservicesdata,
    content,
    title,
    serviceDetailBannerTitle,
    serviceDetailBannerText,
    category,
    servicesImage,
    callToActionButtonText,
    callToActionText,
    descriptionTitle,
    howItWork1stRowImage,
    howItWork1stRowText,
    howItWork1stRowTitle,
    howItWork2ndRowImage,
    howItWork2ndRowText,
    howItWork2ndRowTitle,
    howItWork3rdRowImage,
    howItWork3rdRowText,
    howItWork3rdRowTitle,
    howItWorkHeading,
    whyUs1stRowImage,
    whyUs1stRowText,
    whyUs1stRowTitle,
    whyUs2ndRowImage,
    whyUs2ndRowText,
    whyUs2ndRowTitle,
    whyUs3rdRowImage,
    whyUs3rdRowText,
    whyUs3rdRowTitle,
    whyUsHeading,
    asdata,
    upselldata,
    information_guide
}) =>{

  const [activepopup, setActivepopup] = useState(0)
  const [stepformdata, setStepformdata] = useState(null)
  const [serviceformdata,setServiceformdata] = useState([])
  const [btnshow,setBtnshow] = useState(false)
  const sec1 = useRef(null)
  const sec2 = useRef(null)
  const sec3 = useRef(null)
  const sec4 = useRef(null)
  const stepformref = useRef(null)
  const handleScroll = (e) =>{
		if(e === `sec1`){
			sec1.current.scrollIntoView({behavior:`smooth`})
		}else if(e === `sec2`){
			sec2.current.scrollIntoView({behavior:`smooth`})
		}else if(e === `sec3`){
			sec3.current.scrollIntoView({behavior:`smooth`})
		}else if(e === `sec4`){
			sec4.current.scrollIntoView({behavior:`smooth`})
		}
	}
  const createjson = (data) => {
    return JSON.stringify(data)
  }
  const handleServicePopup = (e) =>{
    if(e === `next`){
      setActivepopup(activepopup + 1)
    }else if(e === `prev`){
      setActivepopup(activepopup - 1)
    }else if(e === `close`){
      setActivepopup(0)
    }else{
      setActivepopup(e)
    } 
  }
  const handleServiceFormSubmittedData = async(e)=>{
   if(!e.submit){
    setServiceformdata({...serviceformdata, [e.form]:e.data})  
   }else{
    // setServiceformdata({...serviceformdata, [e.form]:e.data}) 
    const ltr = Object.values({...serviceformdata, [e.form]:e.data})   

    let bodydata = createjson({"gatsbyinput":{
      "main_title" : "Service form data",
      "main_data" : ltr
    }})
     await fetch("https://backend.production.tonero.eu/wp-json/form/v1/submit", {
      method: "post",
      headers: {"Content-Type": "application/json; charset=UTF-8" },
      body: bodydata
    })
      .then((res) => {    
        console.log(res,'response')       
      })
      .catch(error => {
        console.log(error,'responsee')       
      }); 
   }
  }
  const handleServicesStep = (e) =>{
   if(e !== ""){
    setActivepopup(1)
    setStepformdata(e)
   }
  }


  const [mbl, setMbl] = useState(false)
    const handleMenu = (e) =>{
        if(e === `open`){
          setMbl(true) 
        }else if(e === `close`){
          setMbl(false) 
        }else if(e === `toggle`){
          setMbl(!mbl) 
        }
    }


    const [mbl2, setMbl2] = useState(false)
    const handleMenu2 = (e) =>{
        if(e === `open`){
          setMbl2(true) 
        }else if(e === `close`){
          setMbl2(false) 
        }else if(e === `toggle`){
          setMbl2(!mbl2) 
        }
    }

const scrollLisFunction=(e)=>{
  if(window !== undefined){
    const wh = window.innerHeight/1.5
    // const ws = window.scrollY
    // const eleheight = sec2.current.offsetHeight
    const ele =stepformref.current.getBoundingClientRect()
    // element.scrollHeight - element.clientHeight;
    console.log(ele.y,'ele.y',ele.height)
    if( ele.y < -ele.height && !btnshow){
      setBtnshow(true)
    }else{
      setBtnshow(false)
    }

  }
}
useEffect(()=>{
  if(window !== undefined){
    window.addEventListener('scroll',scrollLisFunction)
    
  }
  return ()=>{
    window.removeEventListener('scroll',scrollLisFunction)
  }
},[activepopup,serviceformdata])
 
  return(
    <React.Fragment>
      <Helmet>
        <body className={`service-content-ui ${btnshow ? `service-btn-show` : ``}`}/>
      </Helmet>
      <SkeletonTheme color="#c5c5c5" highlightColor="#9d9d9d">
    <div className="header-area">      
    <div className="header-bg" style={{backgroundImage:`url(${servicesImage})`}}>
     <div className="container">
         <div className="header-bredcrumb">
             <nav aria-label="breadcrumb">
               <ol className="breadcrumb">
                 {title 
                 ? 
                 <React.Fragment>
                 <li className="breadcrumb-item">
                   <a href="#">Home</a>
                  </li>
                 <li className="breadcrumb-item active" aria-current="page">{title} </li>
                 </React.Fragment>
                 :
                 <Skeleton width={300}/>}
               </ol>
             </nav>
         </div>
         <div className="header-content">
             <h1 className="font-weight-600 text-white mb-4">
               {serviceDetailBannerTitle 
               ?
               serviceDetailBannerTitle 
              :
              <Skeleton width={300}/>}
              </h1>
             {serviceDetailBannerText
             ?
             <HTMLContent content={serviceDetailBannerText}/>
            :
            <div style={{width:`200px`}}>
            <Skeleton count={2}/>
            </div>}
         </div>
     </div>
 </div>
 </div>  
 
 <main className="main-content">
     <div className="container">
         <div className="row cust-ser1">
             <div className="col-lg-8">
                 <div className="left-content-area mb-4">
                     <div className="bg-white border p-2 mb-4">
                         <div className="section-items">
                             <nav className="navbar navbar-expand-lg navbar-light bg-transparent mb-0 p-0">
                               <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#sectionitems" aria-controls="sectionitems" aria-expanded="false" aria-label="Toggle navigation">
                                 <span className="navbar-toggler-icon"></span>
                               </button>
 
                               <div className="collapse navbar-collapse" id="sectionitems">
                                 <ul className="navbar-nav nav-fill w-100 align-items-start">
                                 
                                   {/* <li className="nav-item active">
                                     <a className="nav-link" href="#">TOP 10</a>
                                   </li> */}
                                   <li className="nav-item">
                                     <span className="nav-link" style={{cursor:'pointer'}} onClick={()=>handleScroll(`sec1`)}>WAAROM TONERO?</span>
                                   </li>
                                   <li className="nav-item">
                                     <span className="nav-link" style={{cursor:'pointer'}} onClick={()=>handleScroll(`sec2`)}>HOE WERKT HET?</span>
                                   </li>
                                   <li className="nav-item">
                                     <span className="nav-link" style={{cursor:'pointer'}} onClick={()=>handleScroll(`sec3`)}>BESCHRIJVING</span>
                                   </li>
                                   <li className="nav-item">
                                     <span className="nav-link" style={{cursor:'pointer'}} onClick={()=>handleScroll(`sec4`)}>BEDRIJVEN</span>
                                   </li>
                                   {/* <li className="nav-item">
                                     <a className="nav-link" href="#">REVIEWS</a>
                                   </li> */}
                                 </ul>
                               </div>
                             </nav>
                         </div>
                     </div>

                     <div className="sectiontwo mb-4" id="sectiontwo" ref={sec2}>
                         <div className="hoe-werk">
                             <div className="card">
                                 <div className="card-header section-title bg-white">
                                   <h4 className="font-weight-700 mb-0">{howItWorkHeading || <Skeleton/>}</h4></div>
                                 <div className="card-body bg-white">
                                     <div className="media mb-1">
                                       {howItWork1stRowImage
                                       ?
                                       <img src={howItWork1stRowImage} className="mr-4 align-self-center"/>
                                       :
                                       <span style={{marginRight:`10px`}}>
                                       <Skeleton circle={true} height={50} width={50}/>
                                       </span>}
                                       <div className="media-body pt-3">
                                         <h5 className="font-weight-600 mt-0 mb-0">{howItWork1stRowTitle || <Skeleton/>}</h5>
                                         <p className="mb-1">{howItWork1stRowText || <Skeleton/>}</p>
                                       </div>
                                     </div>
                                     <div className="media mb-1">
                                       {howItWork2ndRowImage
                                       ?
                                       <img src={howItWork2ndRowImage} className="mr-4 align-self-center"/>
                                       :
                                       <span style={{marginRight:`10px`}}>
                                       <Skeleton circle={true} height={50} width={50}/>
                                       </span>}
                                       <div className="media-body pt-3">
                                         <h5 className="font-weight-600 mt-0 mb-0">{howItWork2ndRowTitle || <Skeleton/>}</h5>
                                          <p className="mb-1">{howItWork2ndRowText || <Skeleton/>}</p>
                                       </div>
                                     </div>
                                     <div className="media mb-1">
                                       {howItWork3rdRowImage
                                       ?
                                       <img src={howItWork3rdRowImage} className="mr-4 align-self-center"/>
                                       :
                                       <span style={{marginRight:`10px`}}>
                                       <Skeleton circle={true} height={50} width={50}/>
                                       </span>}
                                       <div className="media-body pt-3">
                                         <h5 className="font-weight-600 mt-0 mb-0">{howItWork3rdRowTitle || <Skeleton/>}</h5>
                                         <p className="mb-1">{howItWork3rdRowText || <Skeleton/>}</p>
                                       </div>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>


                     
                     
                     <div className={`sectionthree mb-4 ${mbl ? `visiblemessage` : ``}`}  id="sectionthree" ref={sec3}>
                         <div className={`beschrijving`} >
                             <div className="card">
                                 <div className="card-header section-title bg-white">
                                   <h4 className="font-weight-700 mb-0">{descriptionTitle || <Skeleton/>}</h4></div>
                                 <div className="card-body bg-white">
                                     {content ? <HTMLContent content={content}/> : <Skeleton count={20}/>}
                                 </div>
                             </div>
                         </div>
                         <button onClick={()=> handleMenu(`toggle`)} className="meer-btn">Meer lezen</button>
                     </div>


                     {faqdata && faqdata.show_faq && faqdata.show_faq[0] && faqdata.show_faq[0].toLowerCase() === "show" && <div className="sectionfive mb-4" id="sectionfive" >

                       <Faq data={faqdata}/>

                      </div>}



                      {suggestedservicesdata && suggestedservicesdata.length !==0 && <div className="sectionsix mb-4" id="sectionsix">
                        <div className="intrested-item">
                                        
                          <UpsellServiceListt asdata={asdata} upselldata={suggestedservicesdata} pageContext={pageContext}/>

                        </div>
                      </div>}


                     <div className="sectionone mb-4" id="sectionone" ref={sec1}>
                         <div className="waarom-tonero">
                             <div className="card">
                                 <div className="card-header section-title bg-white">
                                   <h4 className="font-weight-700 mb-0">{whyUsHeading || <Skeleton />}</h4></div>
                                 <div className="card-body bg-white">
                                     <div className="media mb-3">
                                       {whyUs1stRowImage 
                                       ? 
                                       <img src={whyUs1stRowImage} className="mr-4 align-self-center"/> 
                                       : 
                                       <span style={{marginRight:`10px`}}>
                                       <Skeleton circle={true} height={50} width={50}/>
                                       </span>
                                       }
                                       <div className="media-body pt-3">
                                         <h5 className="font-weight-600 mt-0 mb-0">{whyUs1stRowTitle || <Skeleton />}</h5>
                                         <p className="mb-1">{whyUs1stRowText || <Skeleton />}</p>
                                       </div>
                                     </div>
                                     <div className="media mb-3">
                                       {whyUs2ndRowImage
                                       ?
                                         <img src={whyUs2ndRowImage} className="mr-4 align-self-center"/>
                                       :
                                       <span style={{marginRight:`10px`}}>
                                       <Skeleton circle={true} height={50} width={50}/>
                                       </span>}
                                       <div className="media-body pt-3">
                                         <h5 className="font-weight-600 mt-0 mb-0">{whyUs2ndRowTitle || <Skeleton/>}</h5>
                                         <p className="mb-1">{whyUs2ndRowText || <Skeleton/>}</p>
                                       </div>
                                     </div>
                                     <div className="media mb-3">
                                       {whyUs3rdRowImage
                                       ?
                                       <img src={whyUs3rdRowImage} className="mr-4 align-self-center"/>
                                       :
                                       <span style={{marginRight:`10px`}}>
                                       <Skeleton circle={true} height={50} width={50}/>
                                       </span>}
                                       <div className="media-body pt-3">
                                         <h5 className="font-weight-600 mt-0 mb-0">{whyUs3rdRowTitle || <Skeleton/>}</h5>
                                         <p className="mb-1">{whyUs3rdRowText || <Skeleton/>}</p>
                                       </div>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>

               


                      {faqdata && faqdata.show_second_description && faqdata.show_second_description[0] && 
                      faqdata.show_second_description[0].toLowerCase() === "show" && <div className={`sectionseven mb-4 ${mbl2 ? `visiblemessage` : ``}`}  id="sectionseven">
                         <div className="beschrijving">
                             <div className="card">
                                 <div className="card-header section-title bg-white">
                                   <h4 className="font-weight-700 mb-0">Informatiegids</h4></div>
                                 <div className="card-body bg-white">                                     
                                     <HTMLContent content={information_guide}/>
                                 </div>
                             </div>
                         </div>
                         <button onClick={()=> handleMenu2(`toggle`)} className="meer-btn">Meer lezen</button>
                     </div>}


 
                     <div className="registreren-box py-3 px-4 mb-4" ref={sec4}>
                         <div className="d-flex justify-content-between">
                             {callToActionText 
                             ?
                             <React.Fragment>
                               <h4 className="font-weight-600 text-white mb-0">{callToActionText}</h4>
                             <a href="#" className="btn btn-register" id="register">{callToActionButtonText}</a>
                             </React.Fragment>
                             :
                             <span style={{width:`100%`}}>
                             <Skeleton count={2}/></span>}
                         </div>
                     </div>						
                 </div>
             </div>
             <div className="col-lg-4">
             {serviceFormTitle && serviceFormTitle !== null 
                    &&  <div className="select-filter-area" id="service-mobile" ref={stepformref}>
                   
                    <div className="select-box p-4">
                         <h5 className="text-white font-weight-600 mb-4">
                           {serviceFormTitle}
                         </h5>
                          {serviceFor && serviceFor !== null 
                          && 
                          <div className="filter-btns">
                            {serviceFor.map((el, i)=>{
                              return (<button key = {`df${i+1}`} 
                                      className="btn btn-install" 
                                      onClick={()=>handleServicesStep(el.step_forms)} >
                                      <p>{el.title}</p>
                                      <span><i className="fas fa-angle-right"></i></span>
                                      </button>)
                            })}
                              
                              
                          </div>}
                     </div>                    
                 </div>} 
             </div>
         </div>  
        <UpsellServiceList asdata={asdata} upselldata={upselldata} pageContext={pageContext}/>

        <a href="#service-mobile" className="service-mbl-btn">Vergelijk Offertes</a>

     </div>
 </main>
 </SkeletonTheme>
 {activepopup > 0 
 && 
 <ServiceStepPopup 
 pageContext={pageContext}
 activepopup={activepopup} 
 handleServicePopup={handleServicePopup} 
 stepformdata={stepformdata} 
 handleServiceFormSubmittedData={handleServiceFormSubmittedData}/>}
 </React.Fragment>
  )
}

const Service = (props) => {
    const [sdata, setSdata] = useState(null)
    const [spdata, setSpdata] = useState(null)
    const [wdata, setWdata] = useState(null)
    const [asdata, setAsdata] = useState(null)
    const [faqdata, setFaqdata] = useState(null)

    useEffect(()=>{
     setTimeout(() => {
      setSdata(props.data.wordpressWpServices)
      setSpdata(props.data.wordpressWpServices.acf)
      setWdata(props.data.wordpressWpWhyus.acf)
      setAsdata(props.data.allWordpressWpServices.edges)
      setFaqdata(props.data.allWordpressWpFaq.edges)
     }, 800);
    },[props,sdata,spdata,wdata,asdata])
    
  return (
    <Layout pagecontext={props.pageContext} searchbar = {true}>
      <YoastData data={sdata ? sdata.yoast : null}/> 
      <SingleServiceTemplate  
        upselldata={spdata && spdata.upsel_services}
        suggestedservicesdata={spdata && spdata.suggested_services}
        asdata={asdata}
        faqdata={spdata}
        pageContext={props.pageContext}
        serviceFormTitle = {spdata && spdata.service_form_title} 
        serviceFor={spdata && spdata.service_for}
        information_guide={spdata && spdata.information_guide}
        content={sdata && sdata.content}
        title={sdata && sdata.title}
        serviceDetailBannerTitle={spdata && spdata.service_detail_banner_title}
        serviceDetailBannerText={spdata && spdata.service_detail_banner_text}
        category={spdata && spdata.category}
        callToActionButtonText={spdata && spdata.call_to_action_button_text}
        callToActionText={spdata && spdata.call_to_action_text}
        descriptionTitle={spdata && spdata.description_title}
        howItWork1stRowImage={wdata && wdata.how_it_work_1st_row_image !==null ? wdata.how_it_work_1st_row_image.source_url : null}
        howItWork1stRowText={wdata && wdata.how_it_work_1st_row_text}
        howItWork1stRowTitle={wdata && wdata.how_it_work_1st_row_title}
        howItWork2ndRowImage={wdata && wdata.how_it_work_2nd_row_image !==null ? wdata.how_it_work_2nd_row_image.source_url : null}
        howItWork2ndRowText={wdata && wdata.how_it_work_2nd_row_text}
        howItWork2ndRowTitle={wdata && wdata.how_it_work_2nd_row_title}
        howItWork3rdRowImage={wdata && wdata.how_it_work_3rd_row_image !==null ? wdata.how_it_work_3rd_row_image.source_url : null}
        howItWork3rdRowText={wdata && wdata.how_it_work_3rd_row_text}
        howItWork3rdRowTitle={wdata && wdata.how_it_work_3rd_row_title}
        howItWorkHeading={wdata && wdata.how_it_work_heading}
        serviceDetailBannerText={spdata && spdata.service_detail_banner_text}
        serviceDetailBannerTitle={spdata && spdata.service_detail_banner_title}
        servicesImage={spdata && spdata.services_image !== null ? spdata.services_image.source_url : null}
        whyUs1stRowImage={wdata && wdata.why_us_1st_row_image !==null ? wdata.why_us_1st_row_image.source_url : null}
        whyUs1stRowText={wdata && wdata.why_us_1st_row_text}
        whyUs1stRowTitle={wdata && wdata.why_us_1st_row_title}
        whyUs2ndRowImage={wdata && wdata.why_us_2nd_row_image !== null ? wdata.why_us_2nd_row_image.source_url : null}
        whyUs2ndRowText={wdata && wdata.why_us_2nd_row_text}
        whyUs2ndRowTitle={wdata && wdata.why_us_2nd_row_title}
        whyUs3rdRowImage={wdata && wdata.why_us_3rd_row_image !== null ? wdata.why_us_3rd_row_image.source_url : null}
        whyUs3rdRowText={wdata && wdata.why_us_3rd_row_text}
        whyUs3rdRowTitle={wdata && wdata.why_us_3rd_row_title}
        whyUsHeading={wdata && wdata.why_us_heading}
        
      />
    </Layout>
  )
}

export default Service

export const pageQuery = graphql`
query ServiceByID($id:String!, $lang: String!) {
  wordpressWpServices(id: {eq: $id}) {
    id
    content
    title
    wpml_current_locale
    slug
    yoast {
      focuskw
      title
      metadesc
      linkdex
      metakeywords
      meta_robots_noindex
      meta_robots_nofollow
      meta_robots_adv
      canonical
      redirect
      opengraph_title
      opengraph_description
      opengraph_image
      twitter_title
      twitter_description
      twitter_image
    }
    acf {
      information_guide
      call_to_action_button_text
      call_to_action_text
      description_title
      service_detail_banner_text
      service_detail_banner_title
      service_form_title
      upsel_services
      suggested_services
      show_second_description
      faq_heading
          show_faq
          faq_list {
            answer
            question
          }
      service_for {
        step_forms
        title
      }
      services_image {
        title
        source_url
      }
    }
  }
  wordpressWpWhyus(wpml_current_locale: {eq: $lang}) {
    acf {
      why_us_heading
      how_it_work_1st_row_image {
        source_url
        title
      }
      how_it_work_1st_row_text
      how_it_work_1st_row_title
      how_it_work_2nd_row_image {
        source_url
        title
      }
      how_it_work_2nd_row_text
      how_it_work_2nd_row_title
      how_it_work_3rd_row_image {
        source_url
        title
      }
      how_it_work_3rd_row_text
      how_it_work_3rd_row_title
      how_it_work_heading
      why_us_1st_row_image {
        source_url
        title
      }
      why_us_1st_row_text
      why_us_1st_row_title
      why_us_2nd_row_image {
        source_url
        title
      }
      why_us_2nd_row_text
      why_us_2nd_row_title
      why_us_3rd_row_image {
        source_url
        title
      }
      why_us_3rd_row_text
      why_us_3rd_row_title
    }
  }
  allWordpressWpServices {
    edges {
      node {
        acf {
          parent_services
          services_image {
            source_url
            title
            slug
          }
        }
        title
        slug
        wordpress_id
      }
    }
  }
  allWordpressWpFaq(filter: {wpml_current_locale: {eq: $lang}}) {
    edges {
      node {
        id
        title
        acf {
          heading
          faq_list {
            answer
            question
          }
        }
        wpml_translations {
          locale
        }
      }
    }
  }
}`  