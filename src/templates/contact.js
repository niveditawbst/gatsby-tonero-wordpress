import React, { useEffect } from 'react'
import TemplateWrapper from '../components/Layout'
import { CardResponse, CardEmail, CardPhone } from '../components/cardtemplatecontact/cardtemplate';
import YoastData from '../components/yoast-data';
import Faq from "../components/thankyou/faq"
import Helmet from 'react-helmet'


export default (props)=>{
    const cdata = props && props.data ? props.data.wordpressPage : null
    const faqdata = props && props.data ? props.data.allWordpressWpFaq : null
    useEffect(()=>{

      
    },[])
    return(
        <TemplateWrapper pagecontext={props.pageContext} searchbar = {true}>
           <Helmet>
          <body className="contact-page"/>
        </Helmet>

           <YoastData data={cdata ? cdata.yoast : null}/> 
        <main className="main-content klantenservice-content-area">

        <div className="contact-header">
              {cdata !== null && cdata.acf !==null && <h3 className="font-weight-700">{cdata.acf.heading}</h3>}
							{cdata !== null && cdata.acf !==null && <p className="mb-5 font-weight-500">{cdata.acf.subheading}</p>}
        </div>

		<div className="container">
			<div className="row justify-content-center">
				<div className="col-md-12 col-12">
						<div className="klantenservice-page-area">							
							<div className="klantenservice-chats">
								<div className="row">
									<div className="col-md-4">
										<div className="snelste-responsetijd-area">
											{cdata !== null && cdata.acf !==null && <CardResponse data={cdata.acf}/>}
										</div>
									</div>
									<div className="col-md-4">
										<div className="email-boxes-aera mt-5">
											{cdata !== null && cdata.acf !==null && <CardEmail data={cdata.acf}/>}
										</div>
									</div>

                  <div className="col-md-4">
										<div className="email-boxes-aera mt-5 m-2">											
											{cdata !== null && cdata.acf !==null && <CardPhone data={cdata.acf}/>}
										</div>
									</div>


								</div>
							</div>



          <hr className="c-horz"></hr>


          {faqdata && faqdata[0] && <Faq data={faqdata[0].node.acf}/> }



							<div className="helo-text-register text-center mb-2">
								<p className="font-weight-500">
                  {/* <span className="bg-white mr-3 d-inline-block">
                      <i className="far fa-question-circle"></i></span>  */}
                      Of keer terug naar de 
                      <a href="#"> homepage</a></p>
							</div>
						</div>					
					</div>
				</div>
			</div>


      <div className="contact-footer"></div>

	</main>



        </TemplateWrapper>
    )
}


export const pageQuery = graphql`
  query contactPageQuery($id: String!,$lang: String!) {
    wordpressPage(id: { eq: $id }) {
      title
      yoast {
        focuskw
        title
        metadesc
        linkdex
        metakeywords
        meta_robots_noindex
        meta_robots_nofollow
        meta_robots_adv
        canonical
        redirect
        opengraph_title
        opengraph_description
        opengraph_image
        twitter_title
        twitter_description
        twitter_image
      }
      acf {
        heading
        subheading
        chat_heading
        chat_title
        chat_subtitle
        chat_button_text
        chat_after_button_text_
        email_title
        email_subtitle
        email_button_text
        email_button_after_text
        phone_title
        phone_subtitle
        phone_no_text
        phone_no_after_text    
      }
    }
    allWordpressWpFaq(filter: {wpml_current_locale: {eq: $lang}}) {
      edges {
        node {
          id
          title
          acf {
            heading
            faq_list {
              answer
              question
            }
          }
          wpml_translations {
            locale
          }
        }
      }
    }
  }
`
