import React from 'react'
import NotFoundPage from '../templates/404'
import {graphql} from 'gatsby'
export default (props)=>{
    return(
        <NotFoundPage data={props.data.allWordpressPage} sdata={props.data.allWordpressWpServices}/>
    )
}


export const pageQueryNotFound = graphql`
  query pageQueryNotFound {
    allWordpressPage(filter: {template: {eq: "404.php"}}) {
      edges {
        node {
          id
          template
          title
          content
          acf {
            heading
          image{
            source_url
          }
          related_services_title
          footer_text
          }
           yoast {
          focuskw
          title
          metadesc
          linkdex
          metakeywords
          meta_robots_noindex
          meta_robots_nofollow
          meta_robots_adv
          canonical
          redirect
          opengraph_title
          opengraph_description
          opengraph_image
          twitter_title
          twitter_description
          twitter_image
        }
        }
      }
    }
    allWordpressWpServices(filter: {wpml_current_locale: {eq: "be_NL"}}) {
      edges {
        node {
          id
          title
          slug
          wpml_current_locale
          acf {
            disable_in_search
            services_image {
              source_url
            }
            parent_services
          }
        }
      }
    }
  }
`
