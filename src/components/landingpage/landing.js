import React from 'react'

import company1 from '../assets/images/icons/icon-landing-b2b-company.svg';
import company2 from '../assets/images/icons/icon-landing-b2b-stars.svg';
import company3 from '../assets/images/icons/icon-landing-star-single.svg';
import company4 from '../assets/images/icons/illustration-people-b2b-landing.svg';



const LandingPage =({
    aff,
    afterregtext,
	contactno,
	phone,
	calltimimg,
	bannerbuttontext,
    heading, 
    subheading, 
    servicetitle, 
    servicesubtitle, 
    servicebox1_title, 
    servicebox2_title, 
    servicebox3_title, 
    servicebox1_description, 
    servicebox2_description, 
	servicebox3_description,
	servicebox1_icon,
	servicebox2_icon, 
	servicebox3_icon,  
    callactiontitle, 
    callactionsubtitle, 
    infotitle, 
	infodescription,
	infobuttontext,
    infobuttonaftertext })=> {
    return(
        
        <div className="landing-page">
            
            <div 
        className="header-bg landing-page-bg affiliate-landing overflow-hidden" 
        style={{backgroundImage : `url(${aff !== null ? aff.source_url: null})`}}>
                  
            	<div className="container">
            		<div className="position-relative">
		            	<div className="header-content">
		            		<div className="row">
		            			<div className="col-lg-6 text-lg-left text-center">
                                    <h1 className="text-white mb-4 font-weight-500">{heading}</h1>
                                    <h5 className="font-weight-400 mb-4 light-text">{subheading}</h5>
				            		<button className="btn btn-register font-weight-500 mt-3">{bannerbuttontext}</button>

                                        {afterregtext&&<p className="mt-3 mb-1 light-text font-weight-300 small-font addi">{afterregtext}</p>}
					            		{contactno&&<p className="phone font-weight-300 text-white mb-2 mt-2 small-font">
										<a href="tel:03 303 72 26" className="text-white">
											<i className="fas fa-phone-alt mr-1"></i> {contactno}</a></p>}
					            		{calltimimg&&<p className="text-white font-weight-300 small-font">{calltimimg}</p>}

				            	</div>
				            	<div className="col-lg-6">
				            		<div className="right-content">
				            			<img src={phone !== null ? phone.source_url: null} className="d-lg-block d-none img-fluid m-auto"/>
				            		</div>
				            	</div>
				            </div>
		            	</div>
		            </div>
	            </div>
            </div>



<main className="main-content landing-content-page">
		<div className="container">
			<div className="row pt-5 mb-4 mt-4 justify-content-center">
				<div className="col-lg-9">
					<div className="text-center mb-5">
    <h3 className="font-weight-700 mb-4">{servicetitle}</h3>
						{/* <div className="border-style"><hr></hr></div> */}
    <h5 className="font-weight-400 mt-4 pt-2">{servicesubtitle}</h5>
					</div>					
				</div>
			</div>
			<div className="landing-page-boxes-area">
				<div className="row mb-5">
					<div className="col-lg-4 text-center">
						<div className="item h-100 p-3">
							<img 
							src={servicebox1_icon !== null 
							? 
							servicebox1_icon.source_url : null} className="mb-3"/>
                            <h4 className="font-weight-700 mt-4 mb-4">{servicebox1_title}</h4>
							<p className="font-weight-500 mb-0">{servicebox1_description}</p>
						</div>
					</div>
					<div className="col-lg-4 text-center">
						<div className="item h-100 p-3">
							<img src={servicebox2_icon !== null 
							? 
							servicebox2_icon.source_url : null}  className="mb-3"/>
							<h4 className="font-weight-700 mt-4 mb-4">{servicebox2_title}</h4>
							<p className="font-weight-500 mb-0">{servicebox2_description}</p>
						</div>
					</div>
					<div className="col-lg-4 text-center">
						<div className="item h-100 p-3">
							<img src={servicebox3_icon !== null 
							? 
							servicebox3_icon.source_url : null}  className="mb-3"/>
							<h4 className="font-weight-700 mt-4 mb-4">{servicebox3_title}</h4>
							<p className="font-weight-500 mb-0">{servicebox3_description}</p>
						</div>
					</div>
				</div>
			</div>
			<div className="landingpage-result-area mb-5 pt-4">
				<div className="row justify-content-center">
					<div className="col-lg-6 text-center">
                        <h3 className="font-weight-700 mb-4">{callactiontitle}</h3>
						{/* <div className="border-style"><hr></hr></div> */}
						<p className="font-weight-500 mt-2 pt-2">{callactionsubtitle}</p>
					</div>
				</div>
			</div>
			<div className="leads-immediately-area mt-3 pb-5 ">
				<div className="row">
					<div className="col-lg-6">
						<img src={company4}  className="img-fluid mb-5"/>
					</div>
					<div className="col-lg-6">
                        <h3 className="font-weight-700 mb-4">{infotitle}</h3>
						<div className="border-style d-lg-block d-none"><hr align="left"></hr></div>
						<div className="border-style d-lg-none d-block"><hr></hr></div>
						<p className="font-weight-500 mt-4 pt-2 l-desc">{infodescription}</p>
						<button className="btn btn-register font-weight-500 mb-3 mt-3 mb-3">{infobuttontext}</button>
	                   <p className="font-weight-600 small-font">{infobuttonaftertext}</p>
					
                        {afterregtext&&<p className="font-weight-400 small-font mb-1 addi">{afterregtext}</p>}
                        {contactno&&<p className="phone font-weight-400 mb-1 small-font"><a href="tel:03 303 72 26"><i className="fas fa-phone-alt mr-1"></i> {contactno}</a></p>}
                        {calltimimg&&<p className="font-weight-400 small-font">{calltimimg}</p>}
                    
                    </div>
				</div>
			</div>
		</div>
	</main>


        </div>

    )
}


export default LandingPage;