import React from 'react'
import logowhite from '../assets/images/icons/logo-white.svg';
import blflag from '../assets/images/icons/flag-belgium.svg';
import nlflag from '../assets/images/icons/flag-netherlands.svg';
import {Link} from 'gatsby'
import chat from '../assets/images/chat-white-24dp.svg';
import mail from '../assets/images/email-24px.svg';
import call from '../assets/images/call-24px.svg';

const CardResponse =({data})=> {
    return(
        
        <div className="card rounded-0">
            <div className="card-header rounded-0">
                <h5 className="text-white font-weight-700 mb-0">{data.chat_heading}</h5>
            </div>
            <div className="card-body p-3">
                <p className="text-black font-weight-700">{data.chat_title}</p>
                <p className="mb-3">{data.chat_subtitle}</p>
                <a href="#" className="btn btn-chat mb-3 font-weight-500"><img src={chat} /> {data.chat_button_text}</a>
                <p>{data.chat_after_button_text_}</p>
            </div>
        </div>

    )
}




const CardEmail =({data})=> {
    return(
        
        <div className="email-box shadow-sm bg-white p-3">
            <p className="text-black font-weight-700">{data.email_title}</p>
            <p className="mb-3">{data.email_subtitle}</p>
            <a href="mailto:info@tonero.com" className="btn btn-white mb-3 font-weight-500"><img src={mail} /> {data.email_button_text}</a>
            <p>{data.email_button_after_text}</p>
        </div>

    )
}



const CardPhone =({data})=> {
    return(
        
        <div className="email-box shadow-sm bg-white p-3 mb-4">
            <p className="text-black font-weight-700">{data.phone_title}</p>
            <p className="mb-3">{data.phone_subtitle}</p>
           
                <a className="btn btn-white mb-3 font-weight-500" href={`tel:${data.phone_no_text}`}>
                    <img src={call} /> {data.phone_no_text}</a>
               
                <p>{data.phone_no_after_text}</p>
        </div>

    )
}


export  {CardResponse, CardEmail, CardPhone};