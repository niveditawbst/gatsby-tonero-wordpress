import React from 'react'
import { Helmet } from 'react-helmet'

const YoastData =(props)=>{
    const windowGlobal = typeof window !== 'undefined' && window
    return(
        <Helmet>
{props.data && props.data.title !=="" && <title>{props.data.title}</title>}
{props.data && props.data.title !=="" && <meta property="og:title" content={props.data.title} />}
{props.data && props.data.metadesc !=="" && <meta property="og:description" content={props.data.metadesc} />}
{props.data && props.data.metadesc !=="" && <meta name="description" content={props.data.metadesc} />}
<meta property="og:site_name" content="Tonero" />
{windowGlobal 
&& 
windowGlobal.location 
&& windowGlobal.location.href 
&& <meta property="og:url" content={windowGlobal.location.href} />}
{windowGlobal 
&& 
windowGlobal.location 
&& windowGlobal.location.href 
&& <link rel="alternate" hreflang={window.location.pathname.split("/")[1]} href={windowGlobal.location.href} />}
{props.data && props.data.twitter_description !=="" && <meta name="twitter:description" content={props.data.twitter_description} />}
{props.data && props.data.twitter_title !=="" && <meta name="twitter:title" content={props.data.twitter_title} />}
{props.data && props.data.twitter_image !=="" && <meta name="twitter:image" content={props.data.twitter_image} />}
        </Helmet>
    )
}
export default YoastData