import React from 'react'
import Helmet from 'react-helmet'
import './all.sass'
import '../components/assets/styles/all.css';
import Header from './header'
import Footer from './footer' 
import faviicon from '../img/favicon.png'

const TemplateWrapper = ({ children, pagecontext, searchbar, setServicesdata }) => (
  <div>
    <Helmet>
    <link
    rel="icon"
    type="image/png"
    href={faviicon}
        />
    </Helmet>
    <Header pageContext={pagecontext} searchbar={searchbar} setServicesdata={setServicesdata}/>
    <div>{children}</div>
    <Footer pageContext={pagecontext}/>
  </div>
)

export default TemplateWrapper
