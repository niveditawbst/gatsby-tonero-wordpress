import React from 'react'
import icon1 from '../assets/images/latest-blog.jpg'
import { HTMLContent } from '../Content'
import Skeleton from 'react-loading-skeleton'
import { Link } from 'gatsby'



const LatestBlog =({data,langurl})=> {

    return(
        <div className="latest-blog-section">
        <div className="container">
             <div className="row">

                 <div className="col-md-12">
                    <div className="how-title">
                        <h3 className="font-weight-700 gray-text">Recente blogberichten</h3>
                     </div>
                 </div>


       {data && data.edges && data.edges.map((el,i)=>{
          if(i<4){
            return(
                <div className="col-12 col-md-6 col-lg-3 ">
                           <Link to={`/${langurl}/blog/${el.node.slug}`}>
                           <div className="latest-box d-block-i-span">
                            {el.node.featured_media ? <img src={el.node.featured_media.source_url}/> : <Skeleton style={{display:'block '}} height={163}/>}            
                                <h3 className="font-weight-700">{el.node.title}</h3> 
                                <HTMLContent content={el.node.excerpt}/>
                            </div>
                            </Link>
                        </div>
                           )
          }
       })}



      


    </div>
</div>
</div>

    )
}


export default LatestBlog;    