import React,{useState,useEffect} from 'react'
import {Link} from 'gatsby'
import logo from '../assets/images/icons/blog-logo.svg'

const ServiceSearch =({title,
						subtitle,
						background,
						search_placeholder,
						search_btn_text,
						after_searchfield_text,
						servicedata})=> {

    return(
		<div className="header-area">
		<div className="header-bg home-page-bg" style={{backgroundImage:`url(${background.source_url})`}}>
		<div className="container">
        <div className="header-content text-center">
	<h1 className="text-white mb-4 font-weight-700">{title}</h1>
	<h5 className="font-weight-400 mb-4 light-text max-w-450">{subtitle}</h5>
	            		
						<SearchForm 
						search_placeholder={search_placeholder}
						search_btn_text={search_btn_text}
						servicedata={servicedata}/>
						<div className="mbl-search-logo">
							<img src={logo} />
						</div>
	            		<p className="mt-4 mb-3 light-text font-weight-200 mbl-hide">{after_searchfield_text}</p>
	            		<ul className="search-tabs p-0  mbl-hide">
	            			<li><a href="#">Zonnepanelen</a></li>
	            			<li><a href="#">Webdesign</a></li>
	            			<li><a href="#">Dakreiniging</a></li>
	            		</ul>
	            	</div>
					</div>
					</div>
					</div>

    )
}


export default ServiceSearch;





const SearchForm = ({search_placeholder, search_btn_text, servicedata}) =>{
	const [searchvalue, setSearchvalue] = useState('')
	const [searchfdata, setSearchfdata] = useState([])
	const handleChange =(e)=>{
		setSearchvalue(e.target.value)
		const dt = servicedata.filter(tts => tts.node.title.toLowerCase().includes(e.target.value.toLowerCase()) && (tts.node.acf.disable_in_search === null || tts.node.acf.disable_in_search.length ===0 ))
		setSearchfdata([...dt])
	}
	useEffect(()=>{


	},[servicedata,searchvalue,searchfdata])
	return(
		<div className="search-form">
		
				<div className="form-group search_icon cust789">
					<input 
					type="text" 
					name="search" 
					value={searchvalue} 
					autoComplete="off"
					onChange={handleChange}
					className="search-input form-control" 
					placeholder={search_placeholder}/>
					 {searchvalue !=='' && <ul className="search-list">
                            {searchfdata.length !==0 ? searchfdata.map((el,i)=>{
                              return <li id={el.node.id} key={i}><Link to={`/${el.node.wpml_current_locale.replace('_','-').toLowerCase()}/${el.node.slug}`}>{el.node.title}</Link></li>
                            }):<li>Niets gevonden</li>}
                            
                          </ul>}
				</div>
				<button className="btn btn-search btn-pink">{search_btn_text}</button>
				<span className="home-search-icon-area">
					<i className="fa fa-search"></i>
				</span>
			
		</div>
	)
}