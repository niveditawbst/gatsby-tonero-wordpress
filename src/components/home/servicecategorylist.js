import React, {useEffect, useState} from 'react'
import { Link } from 'gatsby'
import Skeleton, { SkeletonTheme } from 'react-loading-skeleton'
import Carousel from "react-multi-carousel";
import ReactImageAppear from '../assets/plugin/react-image-appear'

const ServiceCategoryList = (props) => {
  
    const [catname, setCatname] = useState(null)
    const [catservices, setCatservices] = useState(null)
    
        useEffect(()=>{
            if(catservices === null){
                setCatname(props.catname)
                setCatservices(props.catservice)
           
            }
        },[])


        const responsive = {
        
            superLargeDesktop: {
              breakpoint: { max: 4000, min: 3000 },
              items: 4
            },
            desktop: {
              breakpoint: { max: 3000, min: 1024 },
              items: 4
            },
            tablet: {
              breakpoint: { max: 1024, min: 480 },
              items: 3
            },
            mobile: {
              breakpoint: { max: 480, min: 0 },
              items: 2
            }
          };



    return(
        <SkeletonTheme color="#c5c5c5" highlightColor="#9d9d9d">
        <div className="d-block-i-span">
                <h5 className="font-weight-500">{catname || <Skeleton width={300}/>}</h5>
                <div className="mb-5 index-slider">
                {catservices 
                     ? <Carousel 
                        arrows={true} 
                        responsive={responsive}
                        autoPlay={false}
                        draggable={false}
                        swipeable={true}
                        infinite={true}
                        autoPlaySpeed={5000}
                        >
                    
                    {catservices.map((el, i) => {
                        const pt = (!el.node.acf.parent_services 
                        || 
                        el.node.acf.parent_services ==='none') 
                        ? 
                        `${props.langurl}/${el.node.slug}`
                        :
                        `${props.langurl}/${el.node.acf.parent_services}/${el.node.slug}`
                            return(
                            <div className="index-box" id={`${el.slug}-${i+1}`}>
                            <Link to={`/${pt}`}>                           
                                {el.node.acf.services_image 
                                ?
                                 <ReactImageAppear 
                                 src={el.node.acf.services_image.source_url}
                                 className="img-fluid single-img"
                                 animation="fadeIn"
                                 />
                                :
                                <Skeleton height={130}/>}
                                <div className="bg-white">
                                <h6 className="font-weight-700 mt-2">{el.node.title}</h6>
                                </div>
                                </Link>
                            </div>
                           
                            )
                    })}
                
      

             </Carousel>
             :
             <Carousel 
                        arrows={true} 
                        responsive={responsive}
                        autoPlay={true}
                        draggable={false}
                        swipeable={true}
                        infinite={true}
                        autoPlaySpeed={5000}
                        >
                
                 <div className="index-box" id="sk0">
                            <Link to={`/${props.langurl}}`}>                           
                            <Skeleton height={130}/>
                            <div className="bg-white"><h6 className="font-weight-400 mt-2">{<Skeleton width={200}/>}</h6></div>
                                </Link>
                            </div>
                            <div className="index-box" id="sk1">
                            <Link to={`/${props.langurl}}`}>                           
                            <Skeleton height={130}/>
                            <div className="bg-white"><h6 className="font-weight-400 mt-2">{<Skeleton width={200}/>}</h6></div>
                                </Link>
                            </div>
                            <div className="index-box" id="sk2">
                            <Link to={`/${props.langurl}}`}>                           
                            <Skeleton height={130}/>
                            <div className="bg-white"><h6 className="font-weight-400 mt-2">{<Skeleton width={200}/>}</h6></div>
                                </Link>
                            </div>
                            <div className="index-box" id="sk3">
                            <Link to={`/${props.langurl}}`}>                           
                            <Skeleton height={130}/>
                            <div className="bg-white"><h6 className="font-weight-400 mt-2">{<Skeleton width={200}/>}</h6></div>
                                </Link>
                            </div>
                
                </Carousel>}

                </div>
        </div>
        </SkeletonTheme>

    )
}







export {ServiceCategoryList};