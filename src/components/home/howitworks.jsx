import React from 'react'


const HowItWorks =({data})=> {
    return(
        <div className="how-it-works">
        <div className="container">
             <div className="row">

                 <div className="col-md-12">
                    <div className="how-title">
                        <h2 className="font-weight-600">{data.how_it_work_heading}</h2>
                     </div>
                 </div>


        <div className="col-12 col-md-4 col-lg-4 ">
            <div className="how-box">
            <img className="icon-how"  src={data.how_it_work_image_one.source_url}/>            
                <h3 className="font-weight-600">{data.how_it_work_title_one}</h3> 
                <p className="font-weight-400">{data.how_it_work_subtitle_one}</p>
            </div>
        </div>

        <div className="col-12 col-md-4 col-lg-4 ">
            <div className="how-box">
            <img className="icon-how"  src={data.how_it_work_image_two.source_url}/>            
                <h3 className="font-weight-600">{data.how_it_work_title_two}</h3> 
                <p className="font-weight-400">{data.how_it_work_subtitle_two}</p>
            </div>
        </div>


        <div className="col-12 col-md-4 col-lg-4 ">
            <div className="how-box">
            <img className="icon-how"  src={data.how_it_work_image_three.source_url}/>            
                <h3 className="font-weight-600">{data.how_it_work_title_three}</h3> 
                <p className="font-weight-400">{data.how_it_work_subtitle_three}</p>
            </div>
        </div>


    </div>
</div>
</div>

    )
}


export default HowItWorks;    