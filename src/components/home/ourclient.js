import React, {useState} from 'react'

import company1 from '../../components/assets/images/companies/fcr-media.png';
import company2 from '../../components/assets/images/companies/ingenico.png';
import company3 from '../../components/assets/images/companies/lightspeed.png';
import company4 from '../../components/assets/images/companies/groupprottect.png';
import company5 from '../../components/assets/images/companies/kbc.png';

const OurClient =({clients})=> {






    return(
        
        <div className="companies-logos">
            
            {clients && clients.length!==0 && clients.map((el, i) => {
                 return(
                    <img 
                    src={el.node.acf.client_logo_image.source_url} 
                    alt={el.node.acf.client_logo_image.title} 
                    className="img-fluid"/>

                )
            })}
    
        </div>

    )
}


export default OurClient;