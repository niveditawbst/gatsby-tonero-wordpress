import React,{useState,useEffect} from 'react'

import phone from '../../components/assets/images/homepage-phone-demo.png';
import logocolor from '../../components/assets/images/icons/logo-color.svg';
import { Link } from 'gatsby';

const PhoneBlock =({title,
					image,
					logo,
					description,
					btn_text,
					btn_link,
					servicedata})=> {
	const [searchvalue, setSearchvalue] = useState('')
	const [searchfdata, setSearchfdata] = useState([])
	const handleChange =(e)=>{
		setSearchvalue(e.target.value)
		const dt = servicedata.filter(tts => tts.node.title.toLowerCase().includes(e.target.value.toLowerCase()) && (tts.node.acf.disable_in_search === null || tts.node.acf.disable_in_search.length ===0 ))
		setSearchfdata([...dt])
	}
	useEffect(()=>{
	

	},[servicedata,searchvalue,searchfdata])
    return(
        
        <div className="row justify-content-center mt-3 mb-5">
					
					<div className="col-md-5">
						<div className="home-mobile-content-area">
							
							<h2 className="font-weight-700 mb-4">{title}</h2>
							<h6 className="mb-4 font-weight-400">{description}</h6>
							<div className="search-area">
								<form className="my-2 my-lg-0">
									<div className="search_icon">
										<input className="form-control py-2 border-right-0 border"
										value={searchvalue} 
										onChange={handleChange}
										type="search"
										id="search"
										autocomplete="off"
										placeholder="Zoeken.."/>
										    {searchvalue !=='' && <ul className="search-list">
                            {searchfdata.length !==0 ? searchfdata.map((el,i)=>{
                              return <li id={el.node.id} key={i}><Link to={`/${el.node.wpml_current_locale.replace('_','-').toLowerCase()}/${el.node.slug}`}>{el.node.title}</Link></li>
                            }):<li>Niets gevonden</li>}
                            
                          </ul>}
										<span className="search-icon-area">
											<i className="fa fa-search"></i>
										</span>
									</div>
								</form>
							</div>
							<img src={logo} alt="logo" className="mb-4" width="100"/>
						</div>
					</div>

					<div className="col-md-4">
						<div className="mobile-img-home d-md-block d-none">
							<img src={image} alt="homepage-phone-demo" className="img-fluid"/>
						</div>
					</div>
				</div>

    )
}


export default PhoneBlock;