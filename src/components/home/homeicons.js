import React from 'react'

const HomeIcons =({data})=> {
    return(
        <div className="home-icons">
        <div className="container">
        <div className="row">

        <div className="col-12 col-md-3 col-lg-3  pb-3 pb-lg-0">
            <div className="home-media">
            <img className="icon-home"  src={data.featured_icon_one.source_url}/>
                <div className="align-self-center">
                    <p className="home-text">
                        <span className="font-weight-600">{data.featured_heading_one}</span> 
                        <p className="font-weight-400">{data.featured_subheading_one}</p>
                    </p>
                </div>
            </div>
        </div>

        <div className="col-12 col-md-3 col-lg-3  pb-3 pb-lg-0">
            <div className="home-media">
            <img className="icon-home"  src={data.featured_icon_two.source_url}/>
                <div className="align-self-center">
                    <p className="home-text">
                        <span className="font-weight-600">{data.featured_heading_two}</span> 
                        <p className="font-weight-400">{data.featured_subheading_two}</p>
                    </p>
                </div>
            </div>
        </div>

        <div className="col-12 col-md-3 col-lg-3  pb-3 pb-lg-0">
            <div className="home-media">
            <img className="icon-home"  src={data.featured_icon_three.source_url}/>
                <div className="align-self-center">
                    <p className="home-text">
                        <span className="font-weight-600">{data.featured_heading_three}</span> 
                        <p className="font-weight-400">{data.featured_subheading_three}</p>
                    </p>
                </div>
            </div>
        </div>

        <div className="col-12 col-md-3 col-lg-3  pb-3 pb-lg-0">
            <div className="home-media">
            <img className="icon-home"  src={data.featured_icon_four.source_url}/>
                <div className="align-self-center">
                    <p className="home-text">
                        <span className="font-weight-600">{data.featured_heading_four}</span> 
                        <p className="font-weight-400">{data.featured_subheading_four}</p>
                    </p>
                </div>
            </div>
        </div>

    </div>
</div>
</div>

    )
}


export default HomeIcons;    