import React from 'react'


const CallToAction =({title,
                    btn_text,
                    btn_link})=> {
    return(
        
        <div className="row justify-content-center">
            <div className="col-xl-8 col-lg-10 col-md-12 col-12">
                <div className="d-md-flex justify-content-md-between text-center">
                    <h4 className="font-weight-600 text-white mb-md-0 mb-3">{title}</h4>
                    <a href={btn_link === `` ? `#`: btn_link} className="btn btn-register" id="register">{btn_text}</a>
                </div>
            </div>
        </div>

    )
}


export default CallToAction;    