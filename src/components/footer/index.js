import React,{useEffect, useState} from 'react'
import {useStaticQuery, graphql, Link} from 'gatsby';

const Footer = (props) => {
const [cdata, setCdata] = useState(null)
const lang = props && props.pageContext ? props.pageContext.lang : null
const data = useStaticQuery(footerdata)
	useEffect(()=>{
		
	if(data){	
		const dt = data.allWordpressWpFooter.edges.filter(el=>el.node.wpml_current_locale === lang)
		const sdt = data.allWordpressWpSocial.edges.filter(elt=>elt.node.wpml_current_locale === lang)
		const obj = {
			colhead:dt[0].node.acf,
			socdata:[...sdt],
			mdata:data.allWordpressWpApiMenusMenusItems.edges
		}
		setCdata(obj)
	
	}

	},[props])
	// const data =  null
	// const data = useStaticQuery(pageFooter)
	// const fdata = data !== null ? data.wpgraphql : null
	// const sdata = data !== null ? data.wpgraphql.socials : null
	// const menudata = data !== null ? data.wpgraphql.menus.edges : null
	// const fmenu1 = menudata !== null ? menudata.filter(el=> el.node.id === "TWVudTo1")[0].node :null
	// const fmenu2 = menudata !== null ? menudata.filter(el=> el.node.id === "TWVudTo2")[0].node :null
	// const fmenu3 = menudata !== null ? menudata.filter(el=> el.node.id === "TWVudTo3")[0].node :null
	// const fmenu4 = menudata !== null ? menudata.filter(el=> el.node.id === "TWVudTo4")[0].node :null








    return(
        
        <footer className="footer" id="footer">
		
		<div className="footer-area">
			<div className="container">
				<div className="row mb-5 pb-4">
					<div className="col-md-3">
						<div className="footer-logo">
							{cdata !== null && <img src={cdata.colhead.footer_1_heading.source_url} alt={cdata.colhead.footer_1_heading.title} width="logo" className="mb-2"/>}
							<ul className="footer-links p-0">
								<List data={cdata ? cdata.mdata:null} menu={cdata!==null ? cdata.colhead.footer_menu_1:null}/>
								{/* {fmenu1 !== null && fmenu1.menuItems.nodes.map((el,i)=>{
									return <li key={`menu1${i+1}`} id={el.menuItemId}><a href={el.url}>{el.label}</a></li>
								})} */}
								
							</ul>
						</div>
					</div>
					<div className="col-md-3">
						<h3 className="footer-title text-white font-weight-400">{cdata !== null && cdata.colhead.footer_2_heading}</h3>
						<ul className="footer-links flagmenu p-0">
						<List data={cdata ? cdata.mdata:null} menu={cdata!==null ? cdata.colhead.footer_menu_2:null}/>
						{/* {fmenu2 !== null && fmenu2.menuItems.nodes.map((el,i)=>{
									return <li key={`menu2${i+1}`} id={el.menuItemId}><a href={el.url}>{el.label}</a></li>
								})} */}
								
							</ul>
					</div>
					<div className="col-md-3">
						<h3 className="footer-title text-white font-weight-400">{cdata !== null &&  cdata.colhead.footer_3_heading}</h3>
						<ul className="footer-links p-0">
						<List data={cdata ? cdata.mdata:null} menu={cdata!==null ? cdata.colhead.footer_menu_3:null}/>
						{/* {fmenu3 !== null && fmenu3.menuItems.nodes.map((el,i)=>{
									return <li key={`menu3${i+1}`} id={el.menuItemId}><a href={el.url}>{el.label}</a></li>
								})} */}
						</ul>
					</div>
					<div className="col-md-3">
						<h3 className="footer-title text-white font-weight-400">{ cdata !== null &&  cdata.colhead.footer_4_heading}</h3>
						<ul className="footer-links p-0">
						<List data={cdata ? cdata.mdata:null} menu={cdata!==null ? cdata.colhead.footer_menu_4:null}/>
						{/* {fmenu4 !== null && fmenu4.menuItems.nodes.map((el,i)=>{
									return <li key={`menu4${i+1}`} id={el.menuItemId}><a href={el.url}>{el.label}</a></li>
								})} */}
						</ul>
					</div>
				</div>
				<div className="copyright-area">
					<div className="d-flex justify-content-between">
						<p className="copyright-text">{cdata !== null && cdata.colhead.copyright_text}</p>
						<ul className="footer-social-links">
						{cdata !== null && cdata.socdata.reverse().map((el,i)=>{
						return(<a href={el.node.acf.link}><i className={el.node.acf.icon}></i></a>
						)
						})}	
						</ul>
					</div>
				</div>
			</div>
		</div> 		
	</footer>

    )
}


export const footerdata = graphql`
{
	allWordpressWpFooter {
	  edges {
		node {
		  id
		  acf {
			copyright_text
			footer_2_heading
			footer_3_heading
			footer_4_heading
			footer_menu_1
			footer_menu_2
			footer_menu_3
			footer_menu_4
			footer_1_heading {
			  source_url
			  title
			}
		  }
		  wpml_current_locale
		}
	  }
	}
	allWordpressWpSocial {
		edges {
		  node {
			id
			title
			wpml_current_locale
			acf {
			  icon
			  link
			}
		  }
		}
	  }
	  allWordpressWpApiMenusMenusItems {
		edges {
		  node {
			id
			name
			items {
			  title
			  url
			}
		  }
		}
	  }
  }
  `


export default Footer;



export const List =(props)=>{
const [mapdata, setMapdata] = useState(null) 
useEffect(()=>{
	if(props.data && props.menu && mapdata === null){
		 const mdatas = props.data.filter(el=>el.node.name === props.menu)		
		setMapdata(mdatas)
	}
	

},[props, mapdata])
	if(mapdata !== null){
	return(
		<React.Fragment>
		{mapdata.length !==0 && mapdata[0].node.items.map((el,i)=>{
			return(
				<li><Link to={el.url}>{el.title}</Link></li>
			)
		})}
		</React.Fragment>
	)
	}else return null

} 
