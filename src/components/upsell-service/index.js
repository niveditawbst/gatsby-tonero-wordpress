import React, { useEffect, useState } from 'react'
import { Link } from 'gatsby'
import Skeleton from 'react-loading-skeleton'
import Carousel from "react-multi-carousel";

const UpsellServiceList = ({asdata,upselldata,pageContext}) =>{
   const [usdata, setUsdata] = useState([])
   const [prefix, setPrefix] = useState(null)
    useEffect(()=>{
        if(pageContext && pageContext.lang && !prefix){
            setPrefix(pageContext.lang.toLowerCase().replace('_','-'))
        }
        if(upselldata && asdata && usdata.length === 0){
            const datatt = []
            upselldata.forEach(el => {
               let st = asdata.filter(elt=>elt.node.wordpress_id.toString() === el)
               datatt.push(st[0])
            });
            setUsdata([...datatt])
        }
    },[asdata,upselldata,prefix])



    const responsive = {
        
        superLargeDesktop: {
          breakpoint: { max: 4000, min: 3000 },
          items: 4
        },
        desktop: {
          breakpoint: { max: 3000, min: 1024 },
          items: 4
        },
        tablet: {
          breakpoint: { max: 1024, min: 480 },
          items: 3
        },
        mobile: {
          breakpoint: { max: 480, min: 0 },
          items: 2
        }
      };

return(
    <React.Fragment>
    {usdata.length !== 0 ? <div class="images-area">
        <h4 class="text-center font-weight-700 mb-4">Andere toonden ook interesse in</h4>
       <div className="related-slider">
       <div className="mb-5 index-slider">
        <Carousel 
                        arrows={true}
                        responsive={responsive}
                        autoPlay={true}
                        draggable={false}
                        swipeable={true}
                        infinite={true}
                        autoPlaySpeed={5000}
                        >
            {usdata.map((el,i)=>{
              const ur = !el.node.acf.parent_services || el.node.acf.parent_services === "none"
              ?  
              `/${prefix}/${el.node.slug}`                                                                                                                                    
              : 
              `/${prefix}/${el.node.acf.parent_services}/${el.node.slug}`

                return(<div class="realted-box">
                <div class="img-flag">
                    <Link to={ur}>
                    {el.node.acf.services_image ? 
                    <img src={el.node.acf.services_image.source_url} alt="" class="img-fluid single-img"/>:
                    <Skeleton height={175}/>}
                    {/* <img src="assets/images/icons/flag-belgium.svg" alt="" class="country-flag"> */}
                    </Link>
                </div>
                <Link to="">
                <h6 class="font-weight-500 mt-2">{el.node.title}</h6>
                </Link>
                </div>)
            })}
            
        </Carousel>
        </div>
        </div>
    </div>:null}
    </React.Fragment>
)
    
}
export default UpsellServiceList;