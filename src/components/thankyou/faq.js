import React, { useEffect } from 'react'
import { Accordion, AccordionItem, AccordionItemHeading, AccordionItemButton, AccordionItemPanel, } from 'react-accessible-accordion';
import 'react-accessible-accordion/dist/fancy-example.css';


const Faq =({data})=> {
    useEffect(()=>{
   
    },[])
    return(
        <div className="faq-container">

    <h3 className="font-weight-600">{data && data.faq_heading}</h3>

                                <div className="faq-content">

                                {data && data.faq_list
                                &&
                                 <Accordion allowZeroExpanded={true}>
                                     {data.faq_list.map((el,i)=>{
                                    return(
                                        <React.Fragment>
                                            <AccordionItem>
                                          <AccordionItemHeading>
                                              <AccordionItemButton>
                                              {el.question}
                                              </AccordionItemButton>
                                          </AccordionItemHeading>
                                          <AccordionItemPanel>
                                              <p>
                                             {el.answer}
                                              </p>
                                          </AccordionItemPanel>
                                      </AccordionItem>
                                        </React.Fragment>
                                    )
                                }) }
                              
                                    
                                      </Accordion>}

                                </div>

                            </div>

    )
}


export default Faq;    