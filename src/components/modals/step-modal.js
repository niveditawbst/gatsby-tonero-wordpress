import React,{useState,useEffect, useRef} from 'react'
import KeyboardEventHandler from '../plugin/react-keyboard-event-handler';
import { HTMLContent } from '../Content'
import { navigate } from 'gatsby';
const windowGlobal = typeof window !== 'undefined' && window
export const Modal1 =({popup_position,lpopup_position,activepopup,handleServicePopup, data,handleServiceFormSubmittedData})=>{
  const [qa,setQa] = useState(null)
  const [ans, setAns] = useState(null)
  const [error, setError] = useState(false)
  const [focussed, setFocussed] = useState(false)
  const wr = useRef(null)

  const handleChange = (e) =>{
    
    if(e === 'submit'){
    let obj = {
      form:popup_position,
      submit:false,
      data:{question:data[0].node.acf.step_1_heading,
            answer:ans}
    }
    setQa(obj) 
    handleServiceFormSubmittedData(obj)
   
    if(popup_position === lpopup_position){
      navigate(`/${windowGlobal.location.pathname.split("/")[1]}/thank-you`)
    }else{
      setFocussed(false)
      handleServicePopup(`next`)
     
    }
   
    }else{
      if(ans !== null){
        if(data[0].node.acf.field_type === "Select"){
          const old = []
          old.push(e.target.value)
          setAns([...old])
        }else{
          const old = ans
          old.push(e.target.value)
          setAns([...old])
        }   
        
       }else{
         const old = []
         old.push(e.target.value)
         setAns([...old])
       }
    }
  
  }
  useEffect(()=>{
   
    if(activepopup && !focussed){
      setTimeout(() => {
   
        wr.current.focus()
      }, 100);
     
      setFocussed(true)
    }
    if(ans!==null){
      setError(false)
    }

  },[qa,ans,popup_position])
return(
<div 
className={`modal fade multi-steps-modal animate ${activepopup ? `show` : `hide`}`} 
id="exampleModal">
<KeyboardEventHandler
            handleKeys={['enter']}
            onKeyEvent={(key, e) => {if(ans!==null){ handleChange(`submit`)}else{setError(true)}}}>
				  <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
				    <div className="modal-content">
				      <div className="modal-header d-flex justify-content-between">
                <a href="#"
                 className="prev-step-btn"
                 onClick={()=>handleServicePopup(`prev`)}
                  style={{visibility:popup_position === 0 ? 'hidden':'visible'}}>
                    <i className="fas fa-arrow-left"></i></a>
				            <h5 className="modal-title" id="exampleModalLabel">{data[0].node.title}</h5>
                        <button 
                        type="button" 
                        className="close" 
                        data-dismiss="modal" 
                        aria-label="Close"
                        onClick={()=>handleServicePopup(`close`)}
                        >
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div className={`modal-body p-0 ${popup_position !==0 ? `animate-left` : ``}`}>
			        	<div className="multi-steps-area">
              <div className="heading text-center pt-5 pb-5">
                <h3>{data[0].node.acf.step_1_heading}</h3>
                </div>
                {error && <small style={{color:'#ff0000',marginLeft:'50px'}}>* Please select an option</small>}
				        	<div className="modal-checkboxes mb-5">
                    {data[0].node.acf.meta_option && data[0].node.acf.meta_option.length !==0 
                    &&
                    data[0].node.acf.meta_option.map((el,i)=>{
                      return(
                      <label htmlFor={`custom${popup_position}RadioInline${i}`} className="item-checkbox">
                      <div className="custom-control 
                      custom-radio 
                      radio-style" >
                      <input 
                      type="radio" 
                      ref={activepopup && i === 0 ? wr : null}
                      id={`custom${popup_position}RadioInline${i}`} 
                      name={data[0].node.acf.field_type === "Select" ? `selecttype${popup_position}` : `multiselecttype${popup_position}${i}`} 
                      value={el.meta_label}
                      onChange={handleChange}
                      className="custom-control-input"/>
                    <label 
                    className="custom-control-label" 
                    >
                      {el.meta_label} 
                    </label>
                    <HTMLContent className="content-desc1" content={el.meta_description}/>
									</div>
								</label>
                      )
                    })}							
							
				        	</div>
                  {lpopup_position === popup_position 
                  &&
                   <div className="calendar-btn text-center mb-5">
                    <button disabled={ans!==null ? false:true} 
                    className="btn btn-calendar"
                    onClick={()=>  navigate(`/${windowGlobal.location.pathname.split("/")[1]}/thank-you`)}>
                      <i className="far fa-calendar mr-3"></i>
                      {data[0].node.acf.step_1_button_text} 
                      </button>
				        	</div>}
				        </div>
				      </div>
              {lpopup_position !== popup_position 
              &&	
              <div className="modal-footer footer-btns">
					        <div className="action-btn">
                    <button 
                    disabled={ans !== null ? false : true} 
                    onClick={()=>{
                      if(ans !== null){
                        handleChange(`submit`)                      
                      }}} 
                    className="btn btn-next btn-block">
                      {data[0].node.acf.step_1_button_text} 
                      <i className="fas fa-long-arrow-alt-right ml-2"></i>
                      </button>
				        	</div>
					    </div>}
				    </div>
				  </div> */}
          </KeyboardEventHandler>
				</div>
)
}


export const Modal7 =({popup_position,lpopup_position,activepopup, handleServicePopup, data,handleServiceFormSubmittedData})=>{
   
  const [formd,setFormd] = useState({})
  const [salactive,setSalactive] = useState(data && data[0].node.acf.salutation[0].option)
  const [thanks, setThanks] = useState(false)
  const [error, setError] = useState(false)
  const voornaam = useRef(null)
  const naam = useRef(null)
  const contactno = useRef(null)
  const mailadres = useRef(null)
  const address = useRef(null)
  const companynaam = useRef(null)
  const description = useRef(null)
  const postcode = useRef(null)

  const handleChange = (e) =>{
    if(e.target.name === 'mailadres'){
      const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
     if(re.test(e.target.value)){
      setFormd({...formd,[e.target.name]:e.target.value})
     }
   
    }else{
      setFormd({...formd,[e.target.name]:e.target.value})
    }
  
  
  
  }
  const pfx = windowGlobal.location.pathname.split("/")[1]
  const handleSubmitForm=(e)=>{
    let formdattt = {...formd,[`voornaam`]:salactive + ' ' + formd.voornaam}
    // e.preventDefault()   
    let obj = {
      form:popup_position,
      submit:true,
      data:{
        formname:"Uw contactgegevens",
        formdata:formdattt}
    }
    // setThanks(true)
    handleServiceFormSubmittedData(obj)
    setTimeout(() => {
      navigate(`/${pfx}/thank-you`)
    }, 1800);
   

  }

  const validate = (e) =>{
if(e === 'focus'){
  voornaam.current.focus()

}else if(e === 'validate'){
  if(!formd.voornaam){
    voornaam.current.focus()
    setError(true)
  }else if(!formd.naam){
    naam.current.focus()
    setError(true)
  }else if(!formd.mailadres){
    mailadres.current.focus()
    setError(true)
  }else if(!formd.contactno){
    contactno.current.focus()
    setError(true)
  }else if(!formd.postcode){
    postcode.current.focus()
    setError(true)
  }else if(!formd.address){
    address.current.focus()
    setError(true)
  }else if(!formd.companynaam){
    companynaam.current.focus()
    setError(true)
  }else if(!formd.description){
    description.current.focus()
    setError(true)
  }else{
    handleSubmitForm()
  }
}
  }
  useEffect(()=>{
    if(!formd.voornaam && activepopup){
    validate('focus')
  }

  },[formd,salactive,thanks,activepopup]) 


  const [menu, setMenu] = useState(false)
    const handleMenu = (e) =>{
        if(e === `open`){
            setMenu(true) 
        }else if(e === `close`){
            setMenu(false) 
        }else if(e === `toggle`){
            setMenu(!menu) 
        }
    }

  return(
        <div 
        className={`modal fade multi-steps-modal animate ${activepopup ? `show` : `hide`}`}  
        id="exampleModal7">
          <KeyboardEventHandler
            handleKeys={['enter']}
            onKeyEvent={(key, e) => {validate('validate')}}>
        <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
          <div className="modal-content">
            <div className="modal-header d-flex justify-content-between">
            <a href="#"
                 className="prev-step-btn"
                 onClick={()=>handleServicePopup(`prev`)}
                  style={{visibility:popup_position === 0 || thanks ? 'hidden':'visible'}}>
                    <i className="fas fa-arrow-left"></i></a>
              <h5 className="modal-title" id="exampleModalLabel7">{data[0].node.title}</h5>
              <button 
              type="button" 
              onClick={()=>handleServicePopup(`close`)} 
              className="close" 
              data-dismiss="modal" 
              aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            {!thanks && <div className={`modal-body p-0 ${popup_position !==0 ? `animate-left` : ``}`}>
                <div className="modal-height">
                  <div className="multi-steps-area">
                      <div className="heading text-center pt-5 pb-5">
                          <h3>{data[0].node.acf.step_4_heading}</h3>
                      </div>
                      <div className="contactgegevens-area">
                          <form
                          method="post"
                          //  onSubmit={handleSubmitForm}                          
                           >
                              <div className="contactgegevens-form px-4">
                                  <div className="row no-gutters">
                                      <div className="col-md-6">
                                          <div className="form-group voornaam">
                                            <input type="text" 
                                            style={{borderBottomColor:!formd.voornaam && error && '#d20000'}}
                                            name="voornaam" 
                                            ref={voornaam}
                                            className="border-right-0" 
                                            onChange={handleChange}
                                            required={true}
                                            />
                                            <label 
                                            htmlFor="input" 
                                            className="control-label">
                                              {data[0].node.acf.first_name_placeholder}</label>
                                            <i className="bar"></i>
                                            {data[0].node.acf.salutation !== null
                                            &&
                                            <div className="mr-mrs-btns">
                                                <div className="btn-group" 
                                                role="group" 
                                                aria-label="Basic example">
                                                {data[0].node.acf.salutation.map((ell,i)=>{
                                                  return <span onClick={()=>setSalactive(data[0].node.acf.salutation[i].option)} className={`btn btn-switch ${salactive===ell.option ? `active` : ``}`}>{ell.option}</span>
                                                })}
                                              </div>
                                            </div>}
                                          </div>
                                      </div>
                                      <div className="col-md-6">
                                          <div className="form-group naam">
                                            <input 
                                            type="text" 
                                            className="naam" 
                                            ref={naam}
                                            style={{borderBottomColor:!formd.naam && error && '#d20000'}}
                                            name="naam"
                                            onChange={handleChange}
                                            required={true}
                                            />
                                            <label 
                                            htmlFor="input" 
                                            className="control-label">{data[0].node.acf.last_name_placeholder}</label><i className="bar"></i>
                                            <div className="icon user-mbl">
                                                <i className="fas fa-user"></i>
                                            </div>
                                          </div>
                                      </div>

                                      <div className="col-md-12">
                                          <div className="form-group">
                                            <input 
                                                 ref={mailadres}
                                            type="email" 
                                            name="mailadres" 
                                            style={{borderBottomColor:!formd.mailadres && error && '#d20000'}}
                                            onChange={handleChange}
                                            required={true}
                                            />
                                            <label 
                                            htmlFor="input" 
                                            className="control-label">
                                              {data[0].node.acf.email_placeholder}
                                            </label><i className="bar"></i>
                                            <div className="icon email">
                                                <i className="fas fa-envelope"></i>
                                            </div>
                                          </div>
                                      </div>
                                      
                                      <div className="col-md-12">
                                          <div className="form-group phone-no-field">
                                            <input 
                                            type="number" 
                                            ref={contactno}
                                            style={{borderBottomColor:!formd.contactno && error && '#d20000'}}
                                            name="contactno" 
                                            onChange={handleChange}
                                            required={true}
                                            />
                                            <label 
                                            htmlFor="input" 
                                            className="control-label">
                                              {data[0].node.acf.phone_no_placeholder_text}
                                            </label><i className="bar"></i>
                                            <div className="icon phone">
                                                <i className="fa fa-phone" style={{transform: `rotate(180deg)`}}></i>
                                            </div>
                                            <div className="country-code">
                                            <li className={`nav-item dropdown ${menu ? `show` : ``}`}>
                                                <span class="nav-link dropdown-toggle">
                                                  <span class={`flag-icon flag-icon-${pfx === `be-nl` ? `be` : `nl`}`}> </span> {pfx === `be-nl` ? `+32` : `+31`}</span>
                                               
                                            </li>
                                            </div>

                                          </div>
                                      </div>
                                   

                                      <div className="col-md-12">
                                          <div className="form-group postcode-input">
                                            <input 
                                            type="number"                                            
                                            name="postcode"  
                                            style={{borderBottomColor:!formd.postcode && error && '#d20000'}}                                           
                                            required={true}
                                            autoComplete="off"
                                            onChange={handleChange}
                                            ref={postcode}
                                            />
                                            <label 
                                            htmlFor="input" 
                                            className="control-label">
                                                {data[0].node.acf.postcode_placeholder}
                                            </label><i className="bar"></i>
                                            <div className="icon postcode">
                                                <i className="fa fa-map-marked-alt"></i>
                                            </div>
                                          </div>
                                      </div>


                                      <div className="col-md-12">
                                          <div className="form-group addd">
                                            <input 
                                            type="text" 
                                            ref={address}
                                            style={{borderBottomColor:!formd.address && error && '#d20000'}}
                                            name="address" 
                                            onChange={handleChange}
                                            required={true}
                                            autoComplete="off"
                                            />
                                            <label 
                                            htmlFor="input" 
                                            className="control-label">
                                                {data[0].node.acf.address_placeholder}
                                            </label><i className="bar"></i>
                                            <div className="icon location">
                                                <i className="fa fa-crosshairs"></i>
                                            </div>
                                          </div>
                                      </div>
                                      <div className="col-md-12">
                                          <div className="form-group">
                                            <input
                                             type="text" 
                                             name="companynaam" 
                                             ref={companynaam}
                                             style={{borderBottomColor:!formd.companynaam 
                                              && error && '#d20000'}}
                                             onChange={handleChange}
                                             required={true}
                                             />
                                            <label 
                                            htmlFor="input" 
                                            className="control-label"> {data[0].node.acf.company_name_placeholder}</label>
                                            <i className="bar"></i>
                                            <div className="icon company">
                                                <i className="fas fa-building"></i>
                                            </div>
                                          </div>
                                      </div>

                                      <div className="col-md-12">
                                   
                                          <div className="form-group">
                                            <textarea 
                                            className="form-control" 
                                            ref={description}
                                            type="number" 
                                            style={{borderBottomColor:!formd.description && error && '#d20000'}}
                                            name="description" 
                                            onChange={handleChange}
                                            required={true}
                                            />
                                            <label 
                                            htmlFor="input" 
                                            className="control-label">
                                              {data[0].node.acf.textarea_placeholder}
                                            </label><i className="bar"></i>
                                            <div className="icon note">
                                                <i className="fa fa-sticky-note" ></i>
                                            </div>
                                          </div>
                                      </div>
                                  </div>
                                  <div className="bottom-text row justify-content-center mt-2">
                                      <div className="col-md-9 text-center">
                                          <p>{data[0].node.acf.button_after_text}</p>
                                      </div>
                                  </div>
                                  {lpopup_position === popup_position
                                  &&  
                                  <div className="action-btn">
                                      <span 
                                      className="btn btn-next btn-block"
                                      // type="submit"
                                      onClick={()=>validate('validate')}
                                      >{data[0].node.acf.step_4_button_text} 
                                      </span>
                                  </div>}
                               
                              </div>
                          </form>
                      </div>
                  </div>
              </div>
            </div>}

            {thanks && <div className={`modal-body thank-you p-0 ${thanks ? `animate-left` : ``}`}>
                <div className="modal-height">
                  <div className="multi-steps-area">
                    <div className="thank-you-msg">Thank you.</div>       
                  </div>
              </div>
            </div>}
            {lpopup_position !== popup_position 
            &&
             <div className="modal-footer footer-btns">
                  <div className="action-btn">
                      <a href="#" className="btn btn-next btn-block">{data[0].node.acf.step_4_button_text}  
                      <i className="fas fa-long-arrow-alt-right ml-2"></i></a>
                  </div>
              </div> }
          </div>
        </div>
        </KeyboardEventHandler>
      </div>  
    )
    }







    
// export const Modal4 =({activepopup, handleServicePopup, data})=>{
//   return(
//       <div
//        className={`modal fade multi-steps-modal animate ${activepopup ? `show` : `hide`}`}  
//        id="exampleModal4">
//       <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
//         <div className="modal-content animate-bottom">
//           <div className="modal-header d-flex justify-content-between">
//               <a href="#" onClick={()=>handleServicePopup(`prev`)} className="prev-step-btn"><i className="fas fa-arrow-left"></i></a>
//             <h5 className="modal-title" id="exampleModalLabel4">Kassasystemen</h5>
//             <button type="button" onClick={()=>handleServicePopup(`close`)} className="close" data-dismiss="modal" aria-label="Close">
//               <span aria-hidden="true">&times;</span>
//             </button>
//           </div>
//           <div className="modal-body p-0">
//               <div className="modal-height">
//                 <div className="multi-steps-area">
//                     <div className="heading text-center pt-5 pb-5">
//                         <h3>Bevestigingscode ingeven</h3>
//                         Code verstuurd naar +32 493 384 44 <a href="#">Bewerk</a>
//                     </div>
//                     <div className="row justify-content-center">
//                         <div className="col-md-8">
//                             <div className="enter-opt-area">
//                                 <form action="" method="post">
//                                     <div className="form-group">
//                                         <input type="text" name="opt" value="" className="form-control"/>
//                                     </div>
//                                     <div className="text-center mb-4">
//                                         <div className="timer-opt">00:30</div>
//                                         <a href="#" className="resend-opt">Resend OTP</a>
//                                     </div>
//                                     <div className="text-center">
//                                         <button type="submit"
//                                          className="btn btn-submit-opt" disabled>Aanvraag afronden</button>
//                                     </div>
//                                 </form>
//                             </div>
//                         </div>
//                     </div>
//                 </div>
//             </div>
//           </div>
//            <div className="modal-footer footer-btns">
//                 <div className="action-btn">
//                     <a href="#" onClick={()=>handleServicePopup(`next`)} className="btn btn-next btn-block">Volgende <i className="fas fa-long-arrow-alt-right ml-2"></i></a>
//                 </div>
//             </div>
//         </div>
//       </div>
//     </div>  
//   )
//   }


// export const Modal5 =({activepopup, handleServicePopup, data})=>{
//   return(
//       <div 
//       className={`modal fade multi-steps-modal animate ${activepopup ? `show` : `hide`}`}  
//       id="exampleModal5" >
//       <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
//         <div className="modal-content animate-bottom">
//           <div className="modal-header d-flex justify-content-between">
//               <a href="#" onClick={()=>handleServicePopup(`prev`)} className="prev-step-btn"><i className="fas fa-arrow-left"></i></a>
//             <h5 className="modal-title" id="exampleModalLabel5">Kassasystemen</h5>
//             <button onClick={()=>handleServicePopup(`close`)} type="button" className="close" data-dismiss="modal" aria-label="Close">
//               <span aria-hidden="true">&times;</span>
//             </button>
//           </div>
//           <div className="modal-body p-0">
//               <div className="modal-height">
//                 <div className="multi-steps-area">
//                     <div className="heading text-center pt-5 pb-5">
//                         <h3>Waar moeten de werken worden uitgevoerd</h3>
//                     </div>
//                     <div className="row justify-content-center">
//                         <div className="col-md-8">
//                             <div className="waar-moeten-area">
//                                 <div className="text-center mb-4">
//                                     <button className="btn btn-black btn-block">
//                                         <img src="assets/images/icon-map-pin.png" 
//                                         alt="" 
//                                         width="15" 
//                                         className="mr-2"/> 
//                                         Op mijn huidige locatie</button>
//                                 </div>
//                                 <div className="text-center">
//                                     <button className="btn btn-outline-black btn-block">Op mijn huidige locatie</button>
//                                 </div>
//                             </div>
//                         </div>
//                     </div>
//                 </div>
//             </div>
//           </div>
//             <div className="modal-footer footer-btns">
//                 <div className="action-btn">
//                     <a href="#" onClick={()=>handleServicePopup(`next`)} className="btn btn-next btn-block">Volgende <i className="fas fa-long-arrow-alt-right ml-2"></i></a>
//                 </div>
//             </div>
//         </div>
//       </div>
//     </div>  
//   )
//   }


// export const Modal6 =({activepopup, handleServicePopup, data})=>{
//   return(
//       <div 
//       className={`modal fade multi-steps-modal animate ${activepopup ? `show` : `hide`}`}  
//       id="exampleModal6">
//         <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
//           <div className="modal-content animate-bottom">
//             <div className="modal-header d-flex justify-content-between">
//               <a href="#" onClick={()=>handleServicePopup(`prev`)} className="prev-step-btn"><i className="fas fa-arrow-left"></i></a>
//               <h5 className="modal-title" id="exampleModalLabel6">Kassasystemen</h5>
//               <button type="button" onClick={()=>handleServicePopup(`close`)} className="close" data-dismiss="modal" aria-label="Close">
//                 <span aria-hidden="true">&times;</span>
//               </button>
//             </div>
//             <div className="modal-body p-0">
//               <div className="modal-height">
//                 <div className="multi-steps-area">
//                   <div className="heading text-center pt-5 pb-5">
//                     <h3>vb: Klapdrop 105, 2000 Antwerpen</h3>
//                   </div>
//                   <div className="klapdrop-area">
//                     <ul className="klapdrop-list p-0 mx-4">
//                       <li><i className="fas fa-long-arrow-alt-left mr-3"></i> 
//                                       vb: Klapdrop 105, 2000 Antwerpen</li>
//                     </ul>
//                     <div className="row justify-content-center mt-5">
//                       <div className="col-md-6 text-center klapdrop-text">
//                         <p>U kunt eevnoudig uw adres opzoeken hierboven.<br/>
//                     Gelieve een containerrect adres in te geven</p>
//                   </div>
//                 </div>
//                   </div>
//                 </div>
//             </div>
//             </div>
//             <div className="modal-footer footer-btns">
//                 <div className="action-btn">
//                   <a href="#" onClick={()=>handleServicePopup(`next`)} className="btn btn-next btn-block">Volgende <i className="fas fa-long-arrow-alt-right ml-2"></i></a>
//                 </div>
//             </div>
//           </div>
//         </div>
//       </div>
//   )
//   }

