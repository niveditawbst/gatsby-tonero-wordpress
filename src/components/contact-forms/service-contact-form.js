import React,{useState, useEffect, useRef} from 'react'
import gql from 'graphql-tag'
import { Mutation } from 'react-apollo'

const CONTACT_MUTATION = gql`
mutation CreateSubmissionMutation($clientMutationId: String!, $email: String!, $firstName: String!, $lastName: String!, $message: String!, $phone: String!){
  createSubmission(input: {clientMutationId: $clientMutationId, firstName: $firstName, lastName: $lastName, email: $email, message: $message, phone: $phone}) {
	success
	data
  }
}
`
export const ServiceContactForm = ({data})=>{
    const inputRef = useRef(null);
    const em = useRef(null);
    const ln = useRef(null);
    const ph = useRef(null);
    const msg = useRef(null);
    const sub = useRef(null);
     const [firstNameValue, setFirstNameValue] = useState('n/a')
     const [firstNameFocus, setFirstNameFocus] = useState(false)
     const [lastNameValue, setLastNameValue] = useState('n/a')
     const [emailValue, setEmailValue] = useState('')
     const [messageValue, setMessageValue] = useState('n/a')
     const [phoneValue, setPhoneValue] = useState('n/a')
     const datat = data
     useEffect(()=>{
         if(!firstNameFocus){

        inputRef.current.focus();
        setFirstNameFocus(true)   
        }
    },[firstNameValue, lastNameValue, emailValue, messageValue, phoneValue, firstNameFocus])
    return(
<Mutation mutation={CONTACT_MUTATION}>
{(createSubmission, { loading, error, data }) => (
<React.Fragment>
<form>
    <div className="bg-white p-4 shadow-sm register-box">
        <div className="row">
            <div className="col-md-6">
                <div className="form-group">
                    <label>{datat.acf.first_name_label}</label>
                    <input 
                    type="text" 
                    required = {true}
                    name="voornaam" 
                    className="form-control auth-input" 
                    ref={inputRef}
                    placeholder={datat.acf.first_name_placeholder}
                    onChange={(e)=>{
                        setFirstNameValue(e.target.value)
                    }}
                    onKeyUp={(e)=>{
                        var code = (e.keyCode ? e.keyCode : e.which);
                        if(code == 13) {  
                         ln.current.focus();
                        }
                         
                    }}/>
                </div>
            </div>
            <div className="col-md-6">
                <div className="form-group">
                    <label>{datat.acf.last_name_label}</label>
                    <input 
                    type="text" 
                    name="achternaam" 
                    ref={ln}
                    className="form-control auth-input" 
                    placeholder={datat.acf.last_name_placeholder}
                    onChange={(e)=>{
                        setLastNameValue(e.target.value)
                    }}
                    onKeyUp={(e)=>{
                        var c2 = (e.keyCode ? e.keyCode : e.which);
                        if(c2 == 13) {  
                         em.current.focus();
                        }
                         
                    }}/>
                </div>
            </div>
            <div className="col-md-6">
                <div className="form-group">
                    <label>{datat.acf.email_label}</label>
                    <input 
                    type="email" 
                    name="email" 
                    ref={em}
                    required = {true}
                    placeholder={datat.acf.email_placeholder}
                    onKeyUp={(e)=>{
                        var c1 = (e.keyCode ? e.keyCode : e.which);
                        if(c1 == 13) {
                        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

                        if (reg.test(emailValue) == true) 
                        {
                            ph.current.focus();
                        }  
                         
                        }
                         
                    }}
                    onChange={(e)=>{
                        setEmailValue(e.target.value)
                    }} 
                    className="form-control auth-input" />
                </div>	
            </div>
            <div className="col-md-6">
                <div className="form-group">
                    <label>{datat.acf.phone_label}</label>
                    <input 
                    type="number"
                    name="telefoon"
                    ref={ph}
                    placeholder={datat.acf.phone_placeholder}
                    className="form-control auth-input" 
                    onChange={(e)=>{
                        setPhoneValue(e.target.value)
                    }}
                    onKeyUp={(e)=>{
                        var c = (e.keyCode ? e.keyCode : e.which);
                        if(c == 13) {  
                         msg.current.focus();
                        }
                         
                    }}/>
                </div>
            </div>
            <div className="col-md-12">
                <div className="form-group">
                    <label>{datat.acf.message_label}</label>
                    <textarea 
                    className="form-control auth-textarea no-resize" name="bericht" 
                    placeholder={datat.acf.message_placeholder}
                    ref={msg}
                    required = {true}
                    onChange={(e)=>{
                        setMessageValue(e.target.value)
                    }}
                    onKeyUp={(e)=>{
                        var c3 = (e.keyCode ? e.keyCode : e.which);
                        if(c3 == 13) {  
                            createSubmission({
                            variables: {
                            clientMutationId: 'formSubmissionService',
                            firstName: firstNameValue,
                            lastName: lastNameValue,
                            email: emailValue,
                            message: messageValue,
                            phone: phoneValue
                            }
                        })
                        }
                         
                    }}
                    rows="6"></textarea>
                </div>
            </div>
        </div>
        <div className="text-center">
        
    </div>
    </div>
  
    </form>
    <button type="submit" onClick={async event => {
    event.preventDefault()
    createSubmission({
        variables: {
        clientMutationId: 'formSubmissionService',
        firstName: firstNameValue,
        lastName: lastNameValue,
        email: emailValue,
        message: messageValue,
        phone: phoneValue
        }
    })
    }} ref={sub} className="btn btn-create-account font-weight-400">{datat.acf.button_text}</button>
        <div>

        {loading && <p style={{ padding: '20px' }}>Please wait...</p>}
        {error && (
        <p style={{ padding: '20px' }}>An unknown error has occured, please try again later...</p>
        )}
        {data && <p style={{ padding: '20px' }}>Form submitted successfully</p>}
    </div>
    </React.Fragment>
    )}
    </Mutation>
									
    )
}
