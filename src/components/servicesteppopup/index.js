import React, { useEffect, useState } from 'react'
import {useStaticQuery, graphql} from 'gatsby'
import {Modal1, Modal7} from '../modals/step-modal'
export const ServiceStepPopup =({
                                activepopup, 
                                handleServicePopup, 
                                stepformdata, 
                                handleServiceFormSubmittedData})=>{
    const [serforms,setSerforms] = useState(null)
    const FormStepQuerydata = useStaticQuery(FormStepQuery)

    const handleFormData =()=>{
        const dt = stepformdata.split(",")
        const arrr = []
        dt.forEach(element => {
                arrr.push(element.trim())       
        });
       
        let formdatatta = []
        arrr.forEach((el)=>{ 
                 
                if(FormStepQuerydata.allWordpressWpFormstep1.edges.filter(ele=>ele.node.acf.form_id === el).length > 0){
                    const ft1 = FormStepQuerydata.allWordpressWpFormstep1.edges.filter(ele=>ele.node.acf.form_id === el)
                    let obj = {
                        type:'radio',
                        data: ft1
                    }
                    formdatatta.push(obj)
                }else if(FormStepQuerydata.allWordpressWpFormstep4.edges.filter(ele=>ele.node.acf.form_id === el).length > 0){
                    const ft4 = FormStepQuerydata.allWordpressWpFormstep4.edges.filter(ele=>ele.node.acf.form_id === el)
                    let obj = {
                        type:'contact',
                        data: ft4
                    }
                    formdatatta.push(obj)
                } 
        })
        
        setSerforms([...formdatatta])
       
    }
    useEffect(()=>{
     
        if(serforms === null){
            handleFormData()  
        }  

    },[activepopup,serforms])
    return(
        <React.Fragment>     
            <div className={`modal-backdrop fade ${activepopup > 0 && serforms !== null ? `show` : `hide`}`}></div>
            {serforms && serforms !==null 
            &&
            serforms.map((el,i)=>{
             
                if(el.type === `radio`){
                    return <Modal1 
                    popup_position ={i} 
                    lpopup_position={serforms.length-1} 
                    activepopup={ activepopup === i+1 } 
                    handleServicePopup={handleServicePopup} 
                    data={el.data}
                    handleServiceFormSubmittedData={handleServiceFormSubmittedData}
                    />
                }else if(el.type === `contact`){
                    return  <Modal7 
                    popup_position ={i} 
                    lpopup_position={serforms.length-1} 
                    activepopup={ activepopup === i+1 }
                     handleServicePopup={handleServicePopup} 
                     data={el.data}
                     handleServiceFormSubmittedData={handleServiceFormSubmittedData}
                     />
                }
            })}
        </React.Fragment>
    )
}


const FormStepQuery = graphql`
query StepForm {
  allWordpressWpFormstep1 {
    edges {
      node {
        title
        type
        id
        acf {
          form_id
          step_1_button_text
          step_1_heading     
          field_type
          meta_option {
            meta_description
            meta_label
          }
        }
      }
    }
  }
  allWordpressWpFormstep4 {
    edges {
      node {
        title
        type
        id
        acf {
          address_placeholder
          button_after_text
          company_name_placeholder
          email_placeholder
          first_name_placeholder
          form_id
          salutation {
            option
          }
          last_name_placeholder
          step_4_button_text
          step_4_heading
          phone_no_placeholder_text
          textarea_placeholder
          postcode_placeholder
        }
      }
    }
  }
}
`

