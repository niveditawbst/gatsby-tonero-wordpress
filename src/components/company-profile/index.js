import React,{useState, useEffect, useRef} from 'react'
import { ServiceStepPopup } from '../servicesteppopup'
import { HTMLContent } from '../Content'

const CompanyProfile = ({data, pageContext, serviceFormTitle, serviceFor}) =>{
	const acfdata = data !== null && data.acf ? data.acf : null
	const [activepopup, setActivepopup] = useState(0)
    const [stepformdata, setStepformdata] = useState(null)
    const [serviceformdata,setServiceformdata] = useState([])
	const sec1 = useRef(null)
	const sec2 = useRef(null)

	const handleScroll = (e) =>{
		if(e === `sec1`){
			sec1.current.scrollIntoView({behavior:`smooth`})
		}else if(e === `sec2`){
			sec2.current.scrollIntoView({behavior:`smooth`})
		}
	}
  const createjson = (data) => {
    return JSON.stringify(data)
  }
  const handleServicePopup = (e) =>{
    if(e === `next`){
      setActivepopup(activepopup + 1)
    }else if(e === `prev`){
      setActivepopup(activepopup - 1)
    }else if(e === `close`){
      setActivepopup(0)
    }else{
      setActivepopup(e)
    } 
  }
  const handleServiceFormSubmittedData = async(e)=>{
   if(!e.submit){
    setServiceformdata({...serviceformdata, [e.form]:e.data})  
   }else{
    // setServiceformdata({...serviceformdata, [e.form]:e.data}) 
    const ltr = Object.values({...serviceformdata, [e.form]:e.data})   

    let bodydata = createjson({"gatsbyinput":{
      "main_title" : "Company profile service form data",
      "main_data" : ltr
    }})
     await fetch("https://backend.production.tonero.eu/wp-json/form/v1/submit", {
      method: "post",
      headers: {"Content-Type": "application/json; charset=UTF-8" },
      body: bodydata
    })
      .then((res) => {    
        console.log(res,'response')       
      })
      .catch(error => {
        console.log(error,'responsee')       
      }); 
   }
  }
  const handleServicesStep = (e) =>{
   if(e !== ""){
    setActivepopup(1)
    setStepformdata(e)
   }
  }
useEffect(()=>{
  
},[activepopup,serviceformdata])
    return(
	<React.Fragment>
		 {/* <div className="header-area">      
    <div className="header-bg" style={{backgroundImage:`url(${`acfdata.company_logo.source_url`})`}}>
     <div className="container">
         <div className="header-bredcrumb">
             <nav aria-label="breadcrumb">
               <ol className="breadcrumb">
                 <li className="breadcrumb-item">
                   <a href="#">Home</a>
                  </li>
                 <li className="breadcrumb-item active">leveranciers</li>
                 <li className="breadcrumb-item active" aria-current="page">{acfdata.company_name.toLowerCase()} </li>
               </ol>
             </nav>
         </div>
         <div className="header-content">
             <h1 className="text-white mb-4">{acfdata.company_name}</h1>
             <HTMLContent content={acfdata.address}/>
         </div>
     </div>
 </div>
 </div>   */}
<main className="main-content">
		<div className="container">			
			<div className="row">
				<div className="col-xl-8 col-lg-7">
					<div className="left-content-area mb-4 mt-4">
					
						<div className="belivert-area bg-white p-3 border mb-4 rounded">
							<div className="row">
								<div className="col-xl-8 col-lg-12">
									<div className="media">
									  <img src={acfdata.company_logo.source_url} className="mr-3 ml-3" alt={acfdata.company_logo.title} width="100"/>
									  <div className="media-body">
									    <h5 className="mt-0 font-weight-800 pt-2">{acfdata.company_name}</h5>
									    <h4 className="font-weight-300">{acfdata.address}</h4>									   
									  </div>
									</div>
								</div>
								<div className="col-xl-4 col-lg-12">
									<div className="contact-openmen-button text-center">
										<button className="btn btn-light">{acfdata.contact_button_text}</button>
									</div>
								</div>
							</div>
						</div>
						<div className="bg-white border p-2 mb-4">
							<div className="section-items">
								<nav className="navbar navbar-expand-lg navbar-light bg-transparent mb-0 p-0">
								  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#sectionitems" aria-controls="sectionitems" aria-expanded="false" aria-label="Toggle navigation">
								    <span className="navbar-toggler-icon"></span>
								  </button>

								  <div className="collapse navbar-collapse" id="sectionitems">
								    <ul className="navbar-nav nav-fill w-100 align-items-start">
								      <li className="nav-item active">
								        <span className="nav-link pl-0" onClick={()=>handleScroll(`sec1`)} style={{cursor:`pointer`}}>
											{acfdata.tab_one}
											</span>
								      </li>
								      <li className="nav-item">
								        <span className="nav-link" onClick={()=>handleScroll(`sec1`)} style={{cursor:`pointer`}}>{acfdata.tab_two}</span>
								      </li>
								      {/* <li className="nav-item">
								        <span className="nav-link">{acfdata.tab_three}</span>
								      </li> */}
								     
								    </ul>
								  </div>
								</nav>
							</div>
						</div>
						<div className="sectionone mb-4" id="sectionone" ref={sec1}>
							<div className="over-beliVer">
								<div className="card">
									<div className="card-header section-title bg-white">
                                        <h4 className="mb-0">{acfdata.about_heading}</h4></div>
									<div className="card-body bg-white">
										<h5 className="font-weight-600">{acfdata.about_subheading}</h5>
										<p>{acfdata.about_description}</p>
									</div>
								</div>
							</div>
						</div>
						<div className="sectiontwo mb-4" id="sectiontwo" ref={sec2}>
							<div className="bedrijfslocatie">
								<div className="card">
									<div className="card-header section-title bg-white">
                                        <h4 className="mb-0">{acfdata.business_heading}</h4></div>
									<div className="card-body bg-white p-0 custi78">
                                        <iframe 
                                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d163304.25871575446!2d3.2079777630413315!3d51.03460205601752!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c335eaed7f2991%3A0xfd18e003f6988cb8!2sBelivert!5e0!3m2!1sen!2s!4v1578314862217!5m2!1sen!2s" width="100%"  frameborder="0" style={{border:'0'}} allowfullscreen=""></iframe>
									</div>
								</div>
							</div>
						</div>
					
						
						{/* <div className="registreren-box py-3 px-4 mb-4">
							<div className="d-md-flex justify-content-md-between text-center">
								<h4 className="font-weight-600 text-white mb-md-0 mb-3">Word leverancier bij Tonero</h4>
								<a href="#" className="btn btn-register" id="register">Registreren</a>
							</div>
						</div>						 */}
					</div>
				</div>
				<div className="col-xl-4 col-lg-5 d-none d-lg-block">
				<div className="select-filter-area company-profile-filter">
                    {serviceFormTitle !== null 
                    && 
                    <div className="select-box p-4">
                         <h5 className="text-white font-weight-600 mb-4">
                           {serviceFormTitle}
                         </h5>
                          {serviceFor !== null 
                          && 
                          <div className="filter-btns">
                            {serviceFor.map((el, i)=>{
                              return (<button key = {`df${i+1}`} 
                                      className="btn btn-install" 
                                      onClick={()=>handleServicesStep(el.step_forms)} >
                                      <p>{el.title}</p>
                                      <span><i className="fas fa-angle-right"></i></span>
                                      </button>)
                            })}
                              
                              
                          </div>}
                     </div>}                     
                 </div>
				</div>
			</div>
		</div>
	</main>
	{activepopup > 0 
	&& 
	<ServiceStepPopup 
	pageContext={pageContext}
	activepopup={activepopup} 
	handleServicePopup={handleServicePopup} 
	stepformdata={stepformdata} 
	handleServiceFormSubmittedData={handleServiceFormSubmittedData}/>}
	</React.Fragment>
    )
}
export default CompanyProfile