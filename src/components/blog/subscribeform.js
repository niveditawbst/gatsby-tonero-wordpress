import React from 'react'


const SubscribeForm =(
    {heading, 
    buttontext, 
    buttonaftertext})=> {
    return(
        <div className="col-xl-6 col-lg-8 text-lg-left px-4">
            <h1 className="text-white mb-4 font-weight-600">{heading}</h1>
            <div className="search-form-blog">
                <form action="" method="">
                    <div className="row">
                        <div className="col-lg-8 col-md-8">
                            <div className="form-group mb-1">
                                <input type="text" name="search" value="" className="form-control"/>
                            </div>
                        </div>
                        <div className="col-auto p-0">
                            <button type="submit" className="btn btn-register">{buttontext}</button>
                        </div>
                    </div>
                    
                </form>
            </div>
            <p className="light-text font-weight-300 small-font">{buttonaftertext}</p>
        </div>

    )
}


export default SubscribeForm;