import React, {useState, useEffect} from 'react'
import {useStaticQuery, graphql} from 'gatsby';
import Content,{HTMLContent} from '../../components/Content' 
import blog from '../assets/images/blog.jpg'

const ReleventArticle = () => {




	const [popularmessageData, setPopularMessageData] = useState([
		{title: "Are solar panels still interesting with the new digital meters?", category:"Renewable energy"},
		{title: "The ultimate guide to choosing a web design agency", category:"Business services"},
		{title: "Tips when choosing a new wall color", category:"Living & Beautiful"},
		{title: "Find the right contractors for your project.", category:"Living & Beautiful"}
		
    ])
    


    return(
        
        <div className="right-sidebar pl-5 related-article">

			
			<div className="widget blog-categories-widget mb-2">
				<h5 className="font-weight-600 mb-4 widget-title">RELEVANTE ARTIKELEN</h5>
				<ul className="related-list">				
					{popularmessageData.map((el,i)=>{
						return (
							<li>
								<a href={`#`}>{el.title}</a>
                                <span className="highlight-text">{el.category}</span>
							</li>
						) 
					})}
				</ul>
			</div>




		</div>

    )
}


export default ReleventArticle;