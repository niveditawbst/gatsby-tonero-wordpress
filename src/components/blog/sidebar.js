import React, {useState, useEffect} from 'react'
import {useStaticQuery, graphql, Link} from 'gatsby';
import Content,{HTMLContent} from '../../components/Content' 
import blog from '../assets/images/blog.jpg'

const BlogSidebar = ({mostreadblog,langurl}) => {




	useEffect(()=>{

	},[mostreadblog])


	const [popularmessageData, setPopularMessageData] = useState([
		{title: "Are solar panels still interesting with the new digital meters?", category:"Renewable energy"},
		{title: "The ultimate guide to choosing a web design agency", category:"Business services"},
		{title: "Tips when choosing a new wall color", category:"Living & Beautiful"},
		{title: "Find the right contractors for your project.", category:"Living & Beautiful"}
		
    ])
    


    return(
        
        <div className="right-sidebar pl-5 recents-blogs">

			<div className="widget blog-categories-widget mb-5">
				<h5 className="font-weight-600 mb-4 widget-title">MEEST GELEZEN OP TONERO</h5>


				<div className="latest-blogs">

					{mostreadblog && mostreadblog.map((el,i)=>{
					
						return(
							<Link to={`/${langurl}/blog/${el.slug}`} className="latest-list">
						<div className="latest-info-img">
							<img src={el.image.source_url}/>
							<span>{i+1}</span>
						</div>
						<div className="latest-info">
							<h6 className="font-weight-600">{el.title}</h6>
							<span>{el.count}x keer gelezen</span>
						</div>
					</Link>
						)
					})}

				

				</div>
				
			</div>










		</div>

    )
}


export default BlogSidebar;