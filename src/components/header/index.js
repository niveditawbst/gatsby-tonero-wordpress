import React, {useEffect, useState} from 'react'
import LanguageDropdown from './languagedropdown'
import {useStaticQuery, graphql, Link} from 'gatsby';
import whitelogo from '../assets/images/icons/blog-logo.svg'
const windowGlobal = typeof window !== 'undefined' && window
const Header =({pageContext, searchbar, setServicesdata})=> {
  const lang = pageContext && pageContext.lang
  const pageHeader = graphql`
  query HeaderQuery ($lang : String){
    wordpressWpMainHeader(wpml_current_locale: {eq: $lang}) {
      acf {
        menu {
          menu_link
          menu_name
        }
        logo_dark {
          title
          source_url
        }
        logo_white {
          title
          source_url
        }
        search_placeholder_text
      }
    }
    allWordpressWpServices(filter: {wpml_current_locale: {eq: $lang}}) {
      edges {
        node {
          id
          title
          slug
          wpml_current_locale
          acf {
              disable_in_search
          }
        }
      }
    }
  }
  `
  const [rooturl, setRooturl] = useState(null)
  const data = useStaticQuery(pageHeader)
  const [searchvalue,setSearchvalue] = useState('')
  const [searchfdata, setSearchfdata] = useState([])
  const hdata = data !== null ? data.wordpressWpMainHeader.acf : null
  const sdata = data !== null ? data.allWordpressWpServices.edges.filter(ell=>ell.node.wpml_current_locale === lang) : null
  const menudata = data !== null ? data.wordpressWpMainHeader.acf.menu : null
  const langpre = windowGlobal ? windowGlobal.location.pathname.split("/")[1] : ''
  const handleChange = (e) =>{
    setSearchvalue(e.target.value)

    const dt = sdata.filter(tts => tts.node.title.toLowerCase().includes(e.target.value.toLowerCase()) && (tts.node.acf.disable_in_search === null || tts.node.acf.disable_in_search.length ===0))
    setSearchfdata([...dt])
 
  }

  const [mainopt, setMainopt] = useState(null)
  const handleMenu = (e) =>{
      if(e === `open`){
        setMainopt(true) 
      }else if(e === `close`){
        setMainopt(false) 
      }else if(e === `toggle`){
        setMainopt(!mainopt) 
      }
  }


  useEffect(()=>{
  if(!rooturl){
  setRooturl(`/${pageContext.lang.replace('_','-').toLowerCase()}/`)
  }

  if(setServicesdata && sdata !== null){
    setServicesdata([...sdata])
  }
  },[pageContext,rooturl])



  


    return(
        
        <header id="header">
        <div className="header-area bg-white shadow-sm">
            <nav className="navbar navbar-expand-lg navbar-light bg-transparent">
                <div className="container">
                  <Link className="navbar-brand default_logo_mbl" to={`${rooturl !==null ? rooturl : `/`}`}>
                    <img src={hdata !== null && hdata.logo_dark.source_url} alt={hdata !== null && hdata.logo_dark.title}/>
                  </Link>

                  <Link className="navbar-brand white_logo_mbl" to={`${rooturl !==null ? rooturl : `/`}`}>
                    <img src={whitelogo}/>
                  </Link>
  
                  <button 
                  onClick={()=> handleMenu(`toggle`)} 
                  className="navbar-toggler" 
                  type="button" 
                  data-toggle="collapse" 
                  data-target="#navbar-area" 
                  aria-controls="navbar-area" 
                  aria-expanded="false" 
                  aria-label="Toggle navigation">
                    <i className="fas fa-bars"></i>
                  </button>

                  {searchbar 
                  &&
                  <div className="search-area">
                  <form className="my-2 my-lg-0">
                        <div className="search_icon">
                            <input 
                            className="form-control py-2 border-right-0 border" 
                            type="search" 
                            value={searchvalue}
                            onChange={handleChange}
                            id="search" 
                            placeholder={hdata !== null 
                            && 
                            hdata.search_placeholder_text}
                            autocomplete="off"/>
                            <span className="search-icon-area">
                                <i className="fa fa-search"></i>
                            </span>
                          {searchvalue !=='' 

                          && 

                          <ul className="search-list">
                            {searchfdata.length !==0 
                            ?
                             searchfdata.map((el,i)=>{
                              return <li id={el.node.id} key={i}>
                                <Link to={`${rooturl}${el.node.slug}`}>{el.node.title}</Link></li>
                            }):<li>Niets gevonden</li>}
                            
                          </ul>}
                        </div>
                    </form>
                  </div>}
    
                  <div className={`collapse navbar-collapse ${mainopt ? `show` : ``}`} id="navbar-area">
                    <ul className="navbar-nav ml-auto">
                    {menudata !== null && menudata.map((el,i)=>{
								      	return (
                         <li key={`menudata${i+1}`} className="nav-item">
                          <Link className="nav-link" to={`/${langpre}${el.menu_link}`}>
                            {el.menu_name}
                           </Link>
                        </li>
                        )})}
                      
                      <LanguageDropdown pagecontext={pageContext}/>

                    </ul>
                  </div>
                </div>
            </nav>
        </div>
    </header>


    )
}







export default Header;