import React, {useState, useEffect} from 'react'
import {Link, graphql,useStaticQuery} from 'gatsby'


const LanguageDropdown = ({pagecontext},props) => {
    const [menu, setMenu] = useState(false)
    const langswitcherdata = useStaticQuery(graphql`
    query MyQuery {
        allWordpressAcfLanguageswitcher{
          nodes {
            acf {
              be_NL {
                be_NL
                en_US
                nl_NL
              } 
              en_US {
                be_NL
                en_US
                nl_NL
              }
              nl_NL {
                be_NL
                en_US
                nl_NL
              }
            }
          }
        }
      }
      `)
    const [langopt, setLangopt] = useState(null)
    const handleMenu = (e) =>{
        if(e === `open`){
            setMenu(true) 
        }else if(e === `close`){
            setMenu(false) 
        }else if(e === `toggle`){
            setMenu(!menu) 
        }
    }

    useEffect(()=>{
        const {nodes:lk} = langswitcherdata.allWordpressAcfLanguageswitcher
        if(langopt===null){
            setLangopt(lk[0].acf)
        }
    },[menu,pagecontext,langopt])
  

        if(langopt !== null){
            return(
            
                <li className={`nav-item dropdown ${menu ? `show` : ``}`}>
                    <span 
                    className={`nav-link ${pagecontext.available_langs.length !== 0 ? `dropdown-toggle` : ``}`}
                    style={{cursor:pagecontext.available_langs.length !== 0 ? `pointer`:`default`}}
                    onClick={()=> handleMenu(`toggle`)}
                     id="dropdown09" 
                     ><span className={`flag-icon flag-icon-${pagecontext.lang.split("_")[0] === `en` ? pagecontext.lang.split("_")[1].toLowerCase() : pagecontext.lang.split("_")[0]}`}> </span> 
                     {langopt[pagecontext.lang][0][pagecontext.lang]}</span>
                
                     <div className={`dropdown-menu ${menu ? `show` : ``}`}>
                    {pagecontext.available_langs.length !== 0 
                    &&
                    pagecontext.available_langs.map((el,i)=>{
                        return(                       
                        <Link 
                        className="dropdown-item" 
                        to={pagecontext.pagetemp !== `homepage` 
                        ?
                         `/${el.replace('_','-').toLowerCase()}/${pagecontext.linkpath}/`
                        :
                        `/${el.replace('_','-').toLowerCase()}/`}
                        >
                        <span 
                        className={`flag-icon flag-icon-${el.split("_")[0] === `en` 
                        ? 
                        el.split("_")[1].toLowerCase() 
                        : 
                        el.split("_")[0]}`}/>
                         {langopt[pagecontext.lang][0][el]}
                         </Link>                   
                  
                        )
                    })}
                    </div>
                </li>
        
            )
        }else return null
    }




export default LanguageDropdown;