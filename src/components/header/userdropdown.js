import React, {useEffect, useState} from 'react'



const UserDropdown = () => {
    const [menu, setMenu] = useState(false)
    const handleMenu = (e) =>{
        if(e === `open`){
            setMenu(true) 
        }else if(e === `close`){
            setMenu(false) 
        }else if(e === `toggle`){
            setMenu(!menu) 
        }
    }
    
    useEffect(()=>{

    },[menu])


        return(
            
            <li className={`nav-item dropdown ${menu ? `show` : ``}`}>
                <a 
                className="nav-link dropdown-toggle"
                onClick={()=> handleMenu(`toggle`)} 
                href="#" 
                id="dropdown09" > <i className="far fa-user"></i></a>
                <div className={`dropdown-menu ${menu ? `show` : ``}`}>
                    <a className="dropdown-item" href="#fr">User1</a>
                    <a className="dropdown-item" href="#it">User2</a>
                    <a className="dropdown-item" href="#ru">User3</a>
                </div>
            </li>
    
        )
    }




export default UserDropdown;