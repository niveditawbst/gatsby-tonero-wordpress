import React from 'react'
import { HTMLContent } from '../Content'


const Contact =({data})=> {
    return(
        
        <div className="col">
            <div className="address-box bg-white shadow-sm p-3">
                <h5 className="mb-3 font-weight-500">{data.acf.contact_details_heading}</h5>
                <HTMLContent content={data.acf.contact_details_info} />
                {/* <p className="mb-0 font-weight-500">+32 3 303 72 26<br></br>
                klantenservice@tonero.io<br></br>
                Klapdorp 105, 2000 Antwerpen<br></br>
                België</p> */}
            </div>
        </div>

    )
}


const Supplier =({data})=> {
    return(
        
        <div className="col">
            <div className="address-box bg-white shadow-sm p-3">
                <h5 className="mb-3 font-weight-500">{data.acf.supplier_heading}</h5>
                <HTMLContent content={data.acf.supplier_heading_info} />
                {/* <p className="mb-0 font-weight-500">+32 3 303 72 26
                <br></br>
                sales@tonero.io<br></br>
                Klapdorp 105, 2000 Antwerpen<br></br>
                support@tonero.be</p> */}
            </div>
        </div>

    )
}


export {Contact, Supplier};