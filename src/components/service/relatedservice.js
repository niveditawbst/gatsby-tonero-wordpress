import React, { useEffect } from 'react'
import service1 from '../assets/images/kassasysteem.jpg'
import service2 from '../assets/images/interior.jpg'
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import { Link } from 'gatsby';


const RelatedService =({data,sdata})=> {
    
useEffect(()=>{

},[data])
    const responsive = {
        
        superLargeDesktop: {
          breakpoint: { max: 4000, min: 3000 },
          items: 4
        },
        desktop: {
          breakpoint: { max: 3000, min: 1024 },
          items: 4
        },
        tablet: {
          breakpoint: { max: 1024, min: 480 },
          items: 3
        },
        mobile: {
          breakpoint: { max: 480, min: 0 },
          items: 2
        }
      };


    return(
        <div className="related">
                  <div className="related-service">

                        <div className="container">

                        <h3 className="font-weight-700">{data && data.acf && data.acf.related_services_title}</h3>
                        <div className="mb-5 index-slider">
                        {sdata && <Carousel 
                        arrows={true}
                        responsive={responsive}
                        autoPlay={true}
                        draggable={false}
                        swipeable={true}
                        infinite={true}
                        autoPlaySpeed={5000}
                        >
                        {sdata.edges.map((el,i)=>{
                            if(!el.node.acf.parent_services){
                            return(
                                <div className="relevent" key={`rl${i}`}>
                            <Link to={`/be-nl/${el.node.slug}`}><img src={el.node.acf.services_image.source_url} alt="" className="img-fluid single-img"/>
                                <h6 className="font-weight-700 mt-2">{el.node.title}</h6>
                            </Link>
                        </div>
                            )}
                        })}
                    
                        </Carousel>}
                        </div>

                        </div>

                        </div>




                        <div className="helo-text-register">
                        <p className="font-weight-500">
                        <span className="bg-white mr-3 d-inline-block">
                            <i className="far fa-question-circle"></i>
                            </span>{data && data.acf && data.acf.footer_text}
                            <a href="#"> hier</a>
                        </p>
                        </div>
        </div>

    )
}


export default RelatedService;    